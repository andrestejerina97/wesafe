#DESCRIPCIÓN <br>
Pwa para registro y control de personal con posibles síntomas de COVID, incluye módulo para administradores,usuarios y supervisores

Compatibilidad: Laravel 7 (php 7.4)

#Requisitos:

Descargar  SERVIDOR WEB Y SERVIDOR DE BASE DE DATOS MYSQL(Lamp,xampp,Laragon). <br>
Descargar composer

 
#Pasos:

Situarse en la carpeta del proyecto

cd wesafe

Instalar dependencia
 >composer install

Cambiar nombre de archivo "env" a ".env". Lo ocultara en linux pero puede ser visible con
ctrl+h


Ejecutar para dar permisos a carpeta storage

 >chmown www-data storage/*
 
 >chmod -R 777 storage/*

 >chmown www-data bootstrap/*

 >chmod -R 777 bootstrap/*


Ejecutar las migraciones y seeder

 >php artisan migrate --seed

Crear key de app

>php artisan key:generate


Entrar a http://localhost/wesafe



#Durante el desarrollo:

Si hubieron modificaciones en la base de datos ejecutar:

php artisan migrate:refresh --seed


En caso de no funcionar lo anterior, borrar las tablas de la base de datos y Ejecutar

php artisan migrate --seed

#PARA PRODUCCIÓN

Modificar el .env: app_debug: false, enviroment: production.

Agregar las credenciales de acceso a servidor de email y motor de base de datos