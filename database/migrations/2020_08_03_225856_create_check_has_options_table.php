<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckHasOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_has_options', function (Blueprint $table) {
            $table->bigInteger('daily_checks_id')->unsigned();
            $table->bigInteger('check_options_id')->unsigned();
            $table->tinyInteger('result')->default(0);

            $table->foreign('daily_checks_id')->on('daily_checks')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('check_options_id')->on('check_options')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_has_options');
    }
}
