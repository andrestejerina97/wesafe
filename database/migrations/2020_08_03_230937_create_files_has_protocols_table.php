<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesHasProtocolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_has_protocols', function (Blueprint $table) {
            $table->bigInteger('protocols_id')->unsigned();
            $table->timestamps();
            $table->string('file')->nullable();
            $table->foreign('protocols_id')->on('protocols')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files_has_protocols');
    }
}
