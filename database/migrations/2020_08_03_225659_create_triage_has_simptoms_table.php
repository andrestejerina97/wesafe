<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTriageHasSimptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triage_has_symptoms', function (Blueprint $table) {
            $table->bigInteger('daily_triages_id')->unsigned();
            $table->bigInteger('triage_symptoms_id')->unsigned();

            $table->foreign('daily_triages_id')->on('daily_triages')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('triage_symptoms_id')->on('triage_symptoms')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triage_has_symptoms');
    }
}
