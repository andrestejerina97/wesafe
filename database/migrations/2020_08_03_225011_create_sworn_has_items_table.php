<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSwornHasItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sworn_has_items', function (Blueprint $table) {
            $table->bigInteger('sworn_declarations_id')->unsigned();
            $table->foreign('sworn_declarations_id')->on('sworn_declarations')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('sworn_items_id')->unsigned();
            $table->foreign('sworn_items_id')->on('sworn_items')->references('id')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sworn_has_items');
    }
}
