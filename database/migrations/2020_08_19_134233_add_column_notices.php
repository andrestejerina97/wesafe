<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnNotices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notices', function (Blueprint $table) {
            $table->string('deceased')->nullable();
            $table->string('recovered')->nullable();
            $table->string('negative')->nullable();
            $table->string('positive')->nullable();

        });
    }

    /**
     * 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notices', function (Blueprint $table) {
            $table->dropColumn('deceased');
            $table->dropColumn('recovered');
            $table->dropColumn('negative');
            $table->dropColumn('positive');

        });
    }
}
