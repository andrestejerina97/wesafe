<?php

use App\check_options;
use App\protocol;
use App\sworn_items;
use App\triage_simptoms;
use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        triage_simptoms::create(['name'=>'Sensación de falta de aire o dificultad para respirar en reposo']);
        triage_simptoms::create(['name'=>'Desorientación o confusión']);
        triage_simptoms::create(['name'=>'Desorientación o confusión']);
        triage_simptoms::create(['name'=>'Dolor o fastidio en el pecho']);
        triage_simptoms::create(['name'=>'Coloración azul en los labios, manos o pies']);
        triage_simptoms::create(['name'=>'Disminución del gusto u olfato']);
        triage_simptoms::create(['name'=>'Congestión nasal']);
        triage_simptoms::create(['name'=>'No tengo ninguno de los síntomas']);
        triage_simptoms::create(['name'=>'Otro']);

        check_options::create(['name'=>'¿Tu lugar de trabajo cuenta con alcohol,
        jabón o algún otro desinfectante para manos?']);
        check_options::create(['name'=>'¿Tu lugar de trabajo se encuentra limpio?']);
        check_options::create(['name'=>'¿El día de hoy se te ha tomado la temperatura corporal?']);
        check_options::create(['name'=>'¿Cuentas con todos los equipos de protección personal que te corresponden?']);
        check_options::create(['name'=>'¿Se te ha brindado información sobre el COVID-19, lavado de manos, cubrirse la boca y no tocarse el rostro??']);
        check_options::create(['name'=>'¿Conoces el proceso para reportar un caso de COVID-19 dentro de la empresa?']);

        sworn_items::create(['name'=>'Sensación de alza térmica o fiebre']);
        sworn_items::create(['name'=>'Tos, estornudos o dificultad para respirar']);
        sworn_items::create(['name'=>'Expectoración o flema amarilla o verdosa']);
        sworn_items::create(['name'=>'Contacto con persona(s) con caso confirmado de COVID-19']);
        sworn_items::create(['name'=>'Está tomando alguna medicación']);

        protocol::create([
            'name'=>"Protocolos de limpieza",
        ]);
        protocol::create([
            'name' => "Protocolos de Emergencia",
        ]);
        protocol::create([
            'name' => "Protocolos Médicos",
        ]);
        
    }
}
