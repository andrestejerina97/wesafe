<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        Role::create(['name' => 'supervisor']);

        $user=User::create([
            'name' => "admin",
            'email' => "admin@wesafe.com",
            'password' => Hash::make("admin1234"),
        ]);
        $user->assignRole("admin");
        $user=User::create([
            'name' => "supervisor",
            'email' => "supervisor@wesafe.com",
            'password' => Hash::make("supervisor1234"),
        ]);
        $user->assignRole("supervisor");
        $user=User::create([
            'name' => "user",
            'email' => "user@wesafe.com",
            'password' => Hash::make("user1234"),
        ]);
        
        $user->assignRole("user");

    }
}
