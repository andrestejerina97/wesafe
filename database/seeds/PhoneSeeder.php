<?php

use App\emergency_phone;
use Illuminate\Database\Seeder;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        emergency_phone::create([
            'number'=>'105',
            'description'=>'Policía Nacional',
        ]);
        emergency_phone::create([
            'number'=>'115',
            'description'=>'Cruz Roja',
        ]);
        emergency_phone::create([
            'number'=>'116',
            'description'=>'Bomberos y paramédicos',
        ]);
        emergency_phone::create([
            'number'=>'110',
            'description'=>'Defensa Civil',
        ]);
        emergency_phone::create([
            'number'=>'106',
            'description'=>'SAMU',
        ]);
        emergency_phone::create([
            'number'=>'911',
            'description'=>'Línea integral de seguridad y emergencias',
        ]);
        emergency_phone::create([
            'number'=>'113',
            'description'=>'Línea exclusiva para personas
            con síntomas de covid-19',
        ]);
    }
}
