<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class daily_check extends Model
{
    
    public function scopeDate($query,$param)
    {
        if ($param != null) {
            return $query->where('created_at',$param);
        }
    }

    public function options()
    {
            return $this->hasMany('App\check_has_options','daily_checks_id');
        
    }
}
