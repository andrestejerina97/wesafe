<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qr_register extends Model
{
    //

    public function scopeName($query,$param)
    {
        if ($param != null) {
            return $query->where('name','like', '%'.$param."%");
        }
    }
    
    public function scopeDate($query,$param)
    {
        if ($param != null) {
            return $query->whereDate('register_date',$param);
        }
    }
}
