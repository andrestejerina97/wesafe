<?php

namespace App\Http\Middleware;

use Closure;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Auth;

class Verification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data=$request->all();
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'ci' => $data['ci']])) {
            return $next($request);

        }else{
            return response()->json(array(
                'code'      =>  403,
                'message'   =>  "no verificado"
            ), 403);
        }
        //session(['key' => 'value']);
    }
}
