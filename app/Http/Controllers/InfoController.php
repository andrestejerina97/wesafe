<?php

namespace App\Http\Controllers;


use App\emergency_phone;
use App\sworn_declaration;
use App\sworn_has_items;
use App\sworn_items;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InfoController extends Controller
{
    public function phone()
    {
        $phones=emergency_phone::orderBy('id','asc')->get();
        return view('public.phone.index',compact('phones'));
    }
    public function file()
    {  
       $options=sworn_items::orderBy('id','asc')->get();
        return view('public.report.file_symptom',compact('options'));
    }

    public function storeSworn(Request $request)
    {
      $sworn_declaration=new sworn_declaration();
      $sworn_declaration->users_id=Auth::user()->id;
      if ($request->has('input_medicine')){
        $sworn_declaration->medicines=$request->input('input_medicine');
     }
     if ($request->has('sign')){
      $sworn_declaration->sign=$request->input('sign');
   }
      $sworn_declaration->save();
     if ($request->has('option')) {
        foreach ($request->input('option') as $key => $val) 
        {
          if (in_array($val, $request->input('option')))
           {
            $detail_symptoms = new sworn_has_items();
             $detail_symptoms->sworn_items_id = $val;
             $detail_symptoms->sworn_declarations_id = $sworn_declaration->id;
             $detail_symptoms->save();
           }
        }
      }
      $date=date("Y-m-d");
      $time=date("H:i:s");
      return response()->json([
        'result'=>$sworn_declaration->id,
        'date'=>$date,
        'time'=>$time,
        ]);
  
    }
    public function myqr()
    {
      $user=User::find(Auth::user()->id);
     
      return view('public.qr.index',compact('user'));
   //   Auth::user()->id;
    }
  
}
