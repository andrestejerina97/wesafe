<?php



namespace App\Http\Controllers\supervisor;

use App\Http\Controllers\Controller;

use App\emergency_phone;
use App\qr_register;
use App\sworn_declaration;
use App\sworn_has_items;
use App\sworn_items;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InfoController extends Controller
{
    public function phone()
    {
        $phones=emergency_phone::orderBy('id','asc')->get();
        return view('public.phone.index',compact('phones'));
    }
    public function file()
    {  
        return view('supervisor.report.file_symptom');
    }

    public function storeSworn(Request $request)
    {
      $sworn_declaration=new sworn_declaration();
      $sworn_declaration->users_id=Auth::user()->id;
      if ($request->has('medicines')){
        $sworn_declaration->medicines=$request->input('medicines');

     }
      $sworn_declaration->save();
     if ($request->has('option')) {
        foreach ($request->input('option') as $key => $val) 
        {
          if (in_array($val, $request->input('option')))
           {
            $detail_symptoms = new sworn_has_items();
             $detail_symptoms->sworn_items_id = $val;
             $detail_symptoms->sworn_declarations_id = $sworn_declaration->id;
             $detail_symptoms->save();
           }
        }
      }
      $date=date("Y-m-d");
      $time=date("H:i:s");
      return response()->json([
        'result'=>$sworn_declaration->id,
        'date'=>$date,
        'time'=>$time,
        ]);
  
    }
    public function myqr()
    {
      $user=User::find(Auth::user()->id);
     
      return view('supervisor.qr.index',compact('user'));
   //   Auth::user()->id;
    }
    public function review($date=0)
    {
      if ($date==0) {
        $date=date('Y-m-d');
    }

    $reviews=sworn_declaration::join('users','users.id','sworn_declarations.users_id')
    ->select('users.name','sworn_declarations.created_at','sworn_declarations.id as id')
    ->whereDate('sworn_declarations.created_at',$date)
    ->orderBy('id','desc')->simplePaginate(4);
    return view('supervisor.report.file_symptom_review',compact('reviews'))
    ->with('date',$date);
       
    }
    public function filter($id)
    {   

        $reviews=sworn_declaration::join('users','users.id','sworn_declarations.users_id')
        ->select('users.name','sworn_declarations.*')
        ->where('sworn_declarations.id',$id)
        ->orderBy('sworn_declarations.id','desc')->first();

        $options=sworn_items::orderBy('id','asc')->get();

        $array_option=sworn_has_items::join('sworn_items','sworn_items.id','sworn_has_items.sworn_items_id')
        ->where('sworn_has_items.sworn_declarations_id',$id)
        ->select('sworn_has_items.sworn_items_id')
        ->orderBy('sworn_has_items.sworn_declarations_id','asc')->pluck('sworn_has_items.sworn_items_id')->toArray();

        return view('supervisor.report.file_symptom_filter',compact('reviews','options'))
        ->with('array_option',$array_option)
        ->with('id',$id);
    }
    
    public function lectorQr()
    {
      $user=User::find(Auth::user()->id);
     
      return view('supervisor.qr.lector',compact('user'));
   //   Auth::user()->id;
    }
    public function saveQr(Request $request)
    {
      $id= $request->input('id');
    //  $date=Carbon::now()->format('Y-m-d');
     // $time=Carbon::now()->format('H:i');
     $date=date("Y-m-d");
     $time=date("H:i");
      $qr= new qr_register();
      $qr->users_id=$id;
      $qr->created_users_id= Auth::user()->id;
      $qr->register_date=$date;
      $qr->register_time=$time;
      $qr->save();
      return response()->json([
        'result'=>$qr->id,
        'date'=>$date,
        'time'=>$time,
        ]);
    }
    public function reviewQr(Request $request)
    {
      $date=$request->input('date',null);
      $name=$request->input('search',null); //set value search who input name
      
      $reviews= qr_register::join('users','users.id','qr_registers.users_id')->Date($date)->Name($name)->orderBy('qr_registers.id','desc')->paginate(4);

      return view('supervisor.qr.review',compact('reviews'))
      ->with('date',$date);
    }
}
