<?php


namespace App\Http\Controllers\supervisor;

use App\Http\Controllers\Controller;
use App\check_options;
use App\daily_triage;
use App\triage_has_simptoms;
use App\triage_simptoms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DailyTriageController extends Controller
{
    public function index()
    {
         return view('supervisor.triage.index');
    }

    public function review($date=0)
    {
      if ($date==0) {
        $date=date('Y-m-d');
    }

    $reviews=daily_triage::join('users','users.id','daily_triages.users_id')
    ->select('users.name','daily_triages.created_at','daily_triages.id AS id')
    ->whereDate('daily_triages.created_at',$date)
    ->orderBy('id','desc')->paginate(4);

    return view('supervisor.triage.triage_review',compact('reviews'))
    ->with('date',$date);
       
    }

    public function filter($id)
    {   

        $reviews=daily_triage::join('users','users.id','daily_triages.users_id')
        ->select('users.name','daily_triages.*')
        ->where('daily_triages.id',$id)
        ->orderBy('daily_triages.id','desc')->first();

        $options=triage_has_simptoms::join('triage_symptoms','triage_symptoms.id','triage_has_symptoms.triage_symptoms_id')
        ->where('triage_has_symptoms.daily_triages_id',$id)
        ->orderBy('triage_has_symptoms.daily_triages_id','asc')->get();

        return view('supervisor.triage.triage_filter',compact('reviews','options'))
        ->with('id',$id);
    }

}
