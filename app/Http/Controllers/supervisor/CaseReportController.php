<?php


namespace App\Http\Controllers\supervisor;

use App\Http\Controllers\Controller;
use App\case_report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CaseReportController extends Controller
{
    public function index()
    {
        return view('supervisor.report.index');
    }
    public function review($date=0)
    {   
        if ($date==0) {
            $date=date('Y-m-d');
        }

        $reviews=case_report::join('users','users.id','case_reports.users_id')
        ->select('users.name','case_reports.created_at','case_reports.id as id')
        ->whereDate('case_reports.created_at',$date)
        ->orderBy('id','desc')->simplePaginate(4);

        return view('supervisor.report.report_review',compact('reviews'))
        ->with('date',$date);
    }
    public function filter($id)
    {   

        $reviews=case_report::join('users','users.id','case_reports.users_id')
        ->select('users.name','case_reports.*')
        ->where('case_reports.id',$id)
        ->orderBy('id','desc')->simplePaginate(4);

        return view('supervisor.report.report_filter',compact('reviews'))
        ->with('id',$id);
    }
}
