<?php

namespace App\Http\Controllers;

use App\protocol;
use App\protocol_has_report;
use Illuminate\Http\Request;

class ProtocolController extends Controller
{
    public function index()
    {
        $protocols= protocol::with('reports')->orderBy('id','DESC')->get();
        $reports=protocol_has_report::all();
        return view('public.protocol.index',compact('reports'))
        ->with('protocols',$protocols);
    }
}
