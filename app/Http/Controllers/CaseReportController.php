<?php

namespace App\Http\Controllers;

use App\case_report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CaseReportController extends Controller
{
    public function index()
    {
        return view('public.report.index');
    }
    public function store(Request $request)
    {
        $data=$request->except('_token');
        if($request->has('link_family')){
            if($data['link_family']=='otro')
            $data['link_family']=$request->input('input_link_family');
        }else{
            $data['link_family']='no completado';   
        }
        if($request->has('time_contact')){
            if($data['time_contact']=='otro')
            $data['time_contact']=$request->input('input_time_contact');
        }else{
            $data['time_contact']='no completado';   
        }
      $case_report= case_report::create([
          'name'=> $request->input('name','no completado'),
           'detail'=>  $request->input('name',''),
           'link_family' => $data['link_family'],
           'time_contact' => $data['time_contact'],
           'last_contact'=>  $request->input('last_contact'),
           'users_id'=> Auth::user()->id,

      ]);
     
      $date=date("Y-m-d");
      $time=date("H:i:s");
      return response()->json([
        'result'=>$case_report->id,
        'date'=>$date,
        'time'=>$time,
        ]);
    }
}
