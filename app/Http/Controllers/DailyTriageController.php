<?php

namespace App\Http\Controllers;

use App\check_options;
use App\daily_triage;
use App\triage_has_simptoms;
use App\triage_simptoms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DailyTriageController extends Controller
{
    public function index()
    {
        $options=triage_simptoms::orderBy('id','asc')->get();
        return view('public.triage.index',compact('options'));
    }

    public function store(Request $request)
    {
      $triage=new daily_triage();
      if ($request->has('contact') && ($request->input('contact')=='Si')) {
        $triage->contact="Si";
      }else{
        $triage->contact="No";
      }
      if ($request->has('input_symptom')){
        $triage->symptom_other=$request->input('input_symptom');

     }
      $triage->users_id=Auth::user()->id;
      $triage->save();
     // dd($request->input('symptom'));
     if ($request->has('symptom')) {
        foreach ($request->input('symptom') as $key => $val) 
        {
          if (in_array($val, $request->input('symptom')))
           {
            $detail_symptoms = new triage_has_simptoms();
             $detail_symptoms->triage_symptoms_id = $val;
             $detail_symptoms->daily_triages_id = $triage->id;
             $detail_symptoms->save();
           }
        }
      }
      $date=date("Y-m-d");
      $time=date("H:i:s");
      return response()->json([
        'result'=>$triage->id,
        'date'=>$date,
        'time'=>$time,
        ]);
    }

}
