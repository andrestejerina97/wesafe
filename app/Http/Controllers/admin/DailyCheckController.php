<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\check_has_options;
use App\check_options;
use App\daily_check;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DailyCheckController extends Controller
{
  public function index()
    {
        $options= check_options::orderBy('id','desc')->get();
        return view('admin.report.daily_check',compact('options'));
    }

    public function review($date=0)
    {
      if ($date==0) {
        $date=date('Y-m-d');
    }
        //daily_check::where('created_at',$date)->get();
        //$id=$daily[0];
        $options=check_options::orderBy('id','asc')->get(); 
        
        $reviews=check_has_options::join('daily_checks','daily_checks.id','check_has_options.daily_checks_id')
        ->join('check_options','check_options.id','check_has_options.check_options_id')
        ->select(DB::raw(' count(*) AS filas,check_has_options.check_options_id AS id,DATE_FORMAT(daily_checks.created_at, "%Y-%m-%d") AS date , check_has_options.result AS result'))
        ->groupBy('id','date','result')
        ->having('date','=',$date)
        ->having('check_has_options.result','=',1)
        ->get();
        $total=daily_check::whereDate('daily_checks.created_at',$date)->count();
        $total=$total;

        return view('admin.report.daily_review',compact('options','reviews'))
        ->with('date',$date)
      ->with('total',$total);
    }

    public function reviewYes($id,$date)
    {
        $options=check_options::orderBy('id','desc')->get(); 
        $reviews=daily_check::join('check_has_options','daily_checks.id','check_has_options.daily_checks_id')
        ->join('users','users.id','daily_checks.users_id')
        ->select('users.name','daily_checks.created_at')
        ->whereDate('daily_checks.created_at',$date)
        ->where('check_has_options.check_options_id',$id)
        ->where('check_has_options.result',1)
        ->paginate(4);
     
        $total=$options->count();
        
        return view('admin.report.daily_review_list',compact('options','reviews'))
      ->with('total',$total);
    }
    public function reviewNo($id,$date)
    {
        $options=daily_check::whereDate('daily_checks.created_at',$date)->get(); 
        $reviews=daily_check::join('check_has_options','daily_checks.id','check_has_options.daily_checks_id')
        ->join('users','users.id','daily_checks.users_id')
        ->select('users.name','daily_checks.created_at')
        ->whereDate('daily_checks.created_at',$date)
        ->where('check_has_options.check_options_id',$id)
        ->where('check_has_options.result',0)
        ->paginate(4);
        $total=$options->count();
        
        return view('admin.report.daily_review_list_no',compact('options','reviews'))
      ->with('total',$total);
    }
    public function chart($date=0)
    {
      if ($date==0) {
        $date=date('Y-m-d');
    }
        //daily_check::where('created_at',$date)->get();
        //$id=$daily[0];
        $options=check_options::orderBy('id','asc')->get(); 
        
        $reviews=check_has_options::join('daily_checks','daily_checks.id','check_has_options.daily_checks_id')
        ->join('check_options','check_options.id','check_has_options.check_options_id')
        ->select(DB::raw(' count(*) AS filas,check_has_options.check_options_id AS id,DATE_FORMAT(daily_checks.created_at, "%Y-%m-%d") AS date , check_has_options.result AS result'))
        ->groupBy('id','date','result')
        ->having('date','=',$date)
        ->having('check_has_options.result','=',1)
        ->get();
        $total=daily_check::whereDate('daily_checks.created_at',$date)->count();
        $total=$total;

        return view('admin.report.daily-check-chart',compact('options','reviews'))
        ->with('date',$date)
      ->with('total',$total);
    }
}
