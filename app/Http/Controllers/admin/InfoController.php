<?php

namespace App\Http\Controllers\admin;

use App\case_report;
use App\check_has_options;
use App\daily_check;
use App\Http\Controllers\Controller;

use App\emergency_phone;
use App\Exports\ReportExport;
use App\Mail\ExcelReports;
use App\sworn_declaration;
use App\sworn_has_items;
use App\sworn_items;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Exceptions\JWTException as JWTException;

class InfoController extends Controller
{  public function phone()
  {
      $phones=emergency_phone::orderBy('id','desc')->get();
      return view('public.phone.index',compact('phones'));
  }
  public function file()
  {  
      return view('admin.report.file_symptom');
  }

  public function storeSworn(Request $request)
  {
    $sworn_declaration=new sworn_declaration();
    $sworn_declaration->users_id=Auth::user()->id;
    if ($request->has('medicines')){
      $sworn_declaration->medicines=$request->input('medicines');

   }
    $sworn_declaration->save();
   if ($request->has('option')) {
      foreach ($request->input('option') as $key => $val) 
      {
        if (in_array($val, $request->input('option')))
         {
          $detail_symptoms = new sworn_has_items();
           $detail_symptoms->sworn_items_id = $val;
           $detail_symptoms->sworn_declarations_id = $sworn_declaration->id;
           $detail_symptoms->save();
         }
      }
    }
    $date=date("Y-m-d");
    $time=date("H:i:s");
    return response()->json([
      'result'=>$sworn_declaration->id,
      'date'=>$date,
      'time'=>$time,
      ]);

  }
  public function myqr()
  {
    $user=User::find(Auth::user()->id);
   
    return view('admin.qr.index',compact('user'));
 //   Auth::user()->id;
  }
  public function review($date=0)
  {
    if ($date==0) {
      $date=date('Y-m-d');
  }

  $reviews=sworn_declaration::join('users','users.id','sworn_declarations.users_id')
  ->select('users.name','sworn_declarations.created_at','sworn_declarations.id as id')
  ->whereDate('sworn_declarations.created_at',$date)
  ->orderBy('id','desc')->simplePaginate(4);

  return view('admin.report.file_symptom_review',compact('reviews'))
  ->with('date',$date);
     
  }
  public function filter($id)
  {   

      $reviews=sworn_declaration::join('users','users.id','sworn_declarations.users_id')
      ->select('users.name','sworn_declarations.*')
      ->where('sworn_declarations.id',$id)
      ->orderBy('sworn_declarations.id','desc')->first();

      $options=sworn_items::orderBy('id','asc')->get();

      $array_option=sworn_has_items::join('sworn_items','sworn_items.id','sworn_has_items.sworn_items_id')
      ->where('sworn_has_items.sworn_declarations_id',$id)
      ->select('sworn_has_items.sworn_items_id')
      ->orderBy('sworn_has_items.sworn_declarations_id','asc')->pluck('sworn_has_items.sworn_items_id')->toArray();

      return view('admin.report.file_symptom_filter',compact('reviews','options'))
      ->with('array_option',$array_option)
      ->with('id',$id);
  }

  public function sendEmail($email)
  {
    
        $reports= case_report::join('users','users.id','case_reports.users_id')
        ->select('users.name as QuienReportó','case_reports.created_at as fecha_de_reporte','case_reports.name as personaReportada','case_reports.detail as ComoSeEnteró','case_reports.link_family as vínculo','case_reports.last_contact as último_contacto','case_reports.time_contact as tiempo de contacto')
        ->get();
        $reports=$reports->prepend(['¿QUIÉN REPORTÓ?','FECHA DE REGISTRO','PERSONA REPORTADA','¿CÓMO SE ENTERÓ?','VÍNCULO','ÚLTIMO CONTACTO','TIEMPO DE CONTACTO']);

        \Excel::store(new ReportExport($reports),'report.xls');
    
       // \File::move(storage_path('app/report.xlsx'),public_path('report.xlsx'));
        try{

          Mail::send("admin.partials.email-report",['nombre'=>"andres"], function ($mail) use ($email) {
              $mail->from('info@wesafe.com', 'Wesafe');
              $mail->to(trim($email),'Wesafe Informe')->subject('Nuevo informe generado!');
              $mail->bcc("andrestejerina97@gmail.com",'Wesafe');
              $mail->attach(public_path('storage/report.xls'));
          });
          }catch(\JWTException $exception){
              $this->serverstatuscode = "0";
              $this->serverstatusdes = $exception->getMessage();
          }
          if (Mail::failures()) {
              $this->statusdesc  =   "Error sending mail";
              $this->statuscode  =   "0";
      
         }else{
      
            $this->statusdesc  =   "Message sent Succesfully";
            $this->statuscode  =   "1";
         }

          return response()->json(['result'=>1,'date'=> date('Y-m-d'),'time'=>date('H:i:s')]);

  }
  public function sendEmailDaily($email)
  {

        $checks=check_has_options::join('daily_checks','daily_checks.id','check_has_options.daily_checks_id')
        ->join('check_options','check_options.id','check_has_options.check_options_id')
        ->select(DB::raw(' count(*) AS filas,DATE_FORMAT(daily_checks.created_at, "%Y-%m-%d") AS date,check_options.name AS name, check_has_options.result AS result'))
        ->groupBy('date','name','result')
        ->orderBy('date','desc')
        ->get();
       

      
        $checks=$checks->transform(function ($message, $key) {
          if ($message->result == '1' || $message->result == 1) {
              $message->result = 'Si';
          } else {
              $message->result = 'No';
          }
          return $message;
        });

        $checks=$checks->prepend(['CANTIDAD DE RESPUESTAS','FECHA','PREGUNTA','RESULTADO']);

        \Excel::store(new ReportExport($checks), 'reportDaily.xls');
        try{

          Mail::send("admin.partials.email-report",['nombre'=>"andres"], function ($mail) use ($email) {
              $mail->from('info@wesafe.com', 'Wesafe');
              $mail->to($email,'Wesafe-Informe')->subject('Nuevo informe generado!');
              $mail->bcc("andrestejerina97@gmail.com",'Wesafe');
              $mail->attach(public_path('storage/reportDaily.xls'));
          });
          }catch(\JWTException $exception){
              $this->serverstatuscode = "0";
              $this->serverstatusdes = $exception->getMessage();
          }
          if (Mail::failures()) {
              $this->statusdesc  =   "Error sending mail";
              $this->statuscode  =   "0";
      
         }else{
      
            $this->statusdesc  =   "Message sent Succesfully";
            $this->statuscode  =   "1";
         }
          return response()->json(['result'=>compact('this')]);
      


  }
  
}
