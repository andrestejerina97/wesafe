<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\protocol;
use App\protocol_has_report;
use Illuminate\Http\Request;

class ProtocolController extends Controller
{
    public function index()
    {
        $protocols= protocol::orderBy('id','DESC')->get();
        $reports=protocol_has_report::all();
        return view('public.protocol.index',compact('protocols','reports'));
    }
}
