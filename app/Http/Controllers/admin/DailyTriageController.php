<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\check_options;
use App\daily_triage;
use App\triage_has_simptoms;
use App\triage_simptoms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DailyTriageController extends Controller
{
  public function index()
  {
       return view('admin.triage.inicio');
  }

  public function review($date=0)
  {
    if ($date==0) {
      $date=date('Y-m-d');
  }

  $reviews=daily_triage::join('users','users.id','daily_triages.users_id')
  ->select('users.name','daily_triages.created_at','daily_triages.id as id')
  ->whereDate('daily_triages.created_at',$date)
  ->orderBy('id','desc')->simplePaginate(4);

  return view('admin.triage.triage_review',compact('reviews'))
  ->with('date',$date);
     
  }

  public function filter($id)
  {   

      $reviews=daily_triage::join('users','users.id','daily_triages.users_id')
      ->select('users.name','daily_triages.*')
      ->where('daily_triages.id',$id)
      ->orderBy('daily_triages.id','desc')->first();

      $options=triage_has_simptoms::join('triage_symptoms','triage_symptoms.id','triage_has_symptoms.triage_symptoms_id')
      ->where('triage_has_symptoms.daily_triages_id',$id)
      ->orderBy('triage_has_symptoms.daily_triages_id','asc')->get();

      return view('admin.triage.triage_filter',compact('reviews','options'))
      ->with('id',$id);
  }
  public function chart($date=0)
  {
    if ($date==0) {
      $date=date('Y-m-d');
  }

  $reviews=triage_has_simptoms::join('triage_symptoms','triage_symptoms.id','triage_has_symptoms.triage_symptoms_id')
  ->join('daily_triages','daily_triages.id','triage_has_symptoms.daily_triages_id')
  ->select(DB::raw(' count(*) AS filas,triage_has_symptoms.triage_symptoms_id AS num,DATE_FORMAT(daily_triages.created_at, "%Y-%m-%d") AS date,triage_symptoms.name as name'))
  ->having('date','=',$date)
  ->groupBy('num','date','name')
  ->orderBy('num','asc')
  ->get();

  $options=triage_simptoms::orderBy('id','asc')->get();


  $total=daily_triage::whereDate('daily_triages.created_at',$date)
  ->orderBy('id','desc')->get()->count();

  $last_contact_option_yes=daily_triage::whereDate('daily_triages.created_at',$date) // cuento todos los si y resto al total para saber las respuestas NO
  ->where('contact','Si')
  ->orderBy('id','desc')->get()->count();
  return view('admin.triage.triage_chart',compact('reviews','total','last_contact_option_yes','options','date'));
     
  }
}
