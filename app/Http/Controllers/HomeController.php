<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }
    public function panel()
    {       
         $user = Auth::user();

         if ($user->hasRole('admin')) {
          return redirect()->route('admin');
           # code...
         }
         if ($user->hasRole('supervisor')) {
          return redirect()->route('supervisor');
          # code...
         }
         if ($user->hasRole('user')) {
          return redirect()->route('usuario');
           # code...
         }
     
    }

    public function usuario()
    {
        return view("public.index");
    }
    public function admin()
    {
        return view("admin.index");
    }
    public function supervisor()
    {
        return view("supervisor.index");

    }
}
