<?php

namespace App\Http\Controllers;

use App\notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function index()
    {
        return view('public.notice.index');
    }
    public function comunication()
    {
        return view('public.notice.comunication');
    }
    public function notice()
    {
        $notices= notice::orderBy('created_at','desc')->first();
        return view('public.notice.notice',compact('notices'));
    }
}
