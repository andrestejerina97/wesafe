<?php

namespace App\Http\Controllers;

use App\check_has_options;
use App\check_options;
use App\daily_check;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DailyCheckController extends Controller
{
    public function index()
    {
        $options= check_options::orderBy('id','desc')->get();
        return view('public.report.daily_check',compact('options'));
    }
    public function store(Request $request)
    {
      $daily_check=new daily_check();
      $daily_check->users_id=Auth::user()->id;
      $daily_check->save();
      $options= check_options::get();
    //  $options= compact('options');
      //dd($options);
      foreach ($options as $option) 
        {
          $detail_symptoms = new check_has_options();
          $detail_symptoms->check_options_id = $option->id;
          $detail_symptoms->daily_checks_id = $daily_check->id;
          $detail_symptoms->result =0;

          if ($request->has('option')) {
            foreach ($request->input('option') as $key => $val) 
            {
              if ($val == $option->id)
               {
                $detail_symptoms->result =1;
              }
            }
          }
          $detail_symptoms->save();

        }
    
      $date=date("Y-m-d");
      $time=date("H:i:s");
      return response()->json([
        'result'=>$daily_check->id,
        'date'=>$date,
        'time'=>$time,
        ]);
    }
}
