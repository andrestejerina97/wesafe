<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class protocol extends Model
{
    
    public function reports()
    {
        return $this->hasMany('App\protocol_has_report','protocols_id');

    }
}
