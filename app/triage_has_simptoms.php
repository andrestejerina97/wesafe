<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class triage_has_simptoms extends Model
{
   protected $table='triage_has_symptoms';
   public $timestamps = false;

}
