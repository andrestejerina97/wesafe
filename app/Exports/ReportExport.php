<?php

namespace App\Exports;

use App\case_report;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
class ReportExport implements FromCollection
{
    use Exportable;
 
    protected $reports;
 
    public function __construct($reports = null)
    {
        $this->reports = $reports;
    }
 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->reports ?: case_report::all();
    }

}
