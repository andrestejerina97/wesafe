<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class case_report extends Model
{
    protected $table="case_reports";
    protected $guarded=['id','created_ar','modified_at'];
}
