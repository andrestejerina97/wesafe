<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true,'--seed'=>true]);
});
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::namespace('admin')->name('admin.')->middleware(['auth'])->group(function(){
    Route::get('/admin/triage-diario-index', 'DailyTriageController@index')->name('triage');
    Route::get('/admin/traige-diario-review-/{date?}', 'DailyTriageController@review')->name('triage.review');
    Route::get('/admin/traige-diario-filter-/{id?}', 'DailyTriageController@filter')->name('triage.filter');
    Route::get('/admin/traige-diario/estadisticas/{date?}', 'DailyTriageController@chart')->name('triage.chart');
    Route::get('/admin/traige-diario/send-email/{email}', 'DailyTriageController@sendEmail')->name('triage.send.email');
    Route::get('/admin/triage/enviar-email',function (){
        return view('admin.report.daily-check-email');
    })->name('triage.email');
    //reporte de covid
    Route::get('/admin/reporte-de-covid-19-', 'CaseReportController@index')->name('report.case');
    Route::get('/admin/reporte-de-covid-19-review/{date?}', 'CaseReportController@review')->name('report.case.review');
    Route::get('/admin/reporte-de-covid-19-filter/{id?}', 'CaseReportController@filter')->name('report.case.filter');
    
    //verificación diaria
    Route::get('/admin/verificación-diaria', 'DailyCheckController@index')->name('daily.check');
    Route::get('/admin/verificación-diaria-review', 'DailyCheckController@review')->name('daily.check.review');
    Route::get('/admin/check-si/{id}/{date}', 'DailyCheckController@reviewYes')->name('daily.check.review.yes');
    Route::get('/admin/check-no/{id}/{date}', 'DailyCheckController@reviewNo')->name('daily.check.review.no');
    Route::get('/admin/estadísticas/{date?}', 'DailyCheckController@chart')->name('daily.check.chart');
    Route::get('/admin/enviar-email',function (){
        return view('admin.report.daily-check-email');
    })->name('daily.check.email');
    Route::get('/admin/daily/send-email/{email}', 'InfoController@sendEmailDaily')->name('daily.check.send.email');

    Route::get('/admin/mi-qr', 'InfoController@myQr')->name('myqr');
    //Protocolos
    Route::get('/admin/protocolos', 'ProtocolController@index')->name('protocol');
    //Noticias
    Route::get('/admin/noticias', 'NoticeController@index')->name('notice');
    //Ficha sintomatológica
    Route::get('/admin/file', 'InfoController@file')->name('file');
    Route::get('/admin/file-review/{date?}', 'InfoController@review')->name('file.review');
    Route::get('/admin/file-filter/{id?}', 'InfoController@filter')->name('file.filter');
    Route::get('/admin/file-excel',function(){
        return view('admin.report.report-excel-send');
    })->name('file.send-excel');
    Route::get('/admin/send-email/{email}', 'InfoController@sendEmail')->name('file.send.email');

    //phone
    Route::get('admin/telefonos', 'InfoController@phone')->name('phone');
});

Route::namespace('supervisor')->name('supervisor.')->middleware(['auth'])->group(function () {
   //traige
Route::get('/supervisor/traige-diario-view', 'DailyTriageController@index')->name('triage');
Route::get('/supervisor/traige-diario-review/{date?}', 'DailyTriageController@review')->name('triage.review');
Route::get('/supervisor/traige-diario-filter/{id?}', 'DailyTriageController@filter')->name('triage.filter');

//reporte de covid
Route::get('/supervisor/reporte-de-covid-19-', 'CaseReportController@index')->name('report.case');
Route::get('/supervisor/reporte-de-covid-19-review/{date?}', 'CaseReportController@review')->name('report.case.review');
Route::get('/supervisor/reporte-de-covid-19-filter/{id?}', 'CaseReportController@filter')->name('report.case.filter');

//verificación diaria
Route::get('/supervisor/verificación-diaria', 'DailyCheckController@index')->name('daily.check');
Route::get('/supervisor/verificación-diaria-review-/{date?}', 'DailyCheckController@review')->name('daily.check.review');
Route::get('/supervisor/check-si/{id}/{date}', 'DailyCheckController@reviewYes')->name('daily.check.review.yes');
Route::get('/supervisor/check-no/{id}/{date}', 'DailyCheckController@reviewNo')->name('daily.check.review.no');

Route::get('/supervisor/mi-qr', 'InfoController@myQr')->name('myqr');
//Protocolos
Route::get('/supervisor/protocolos', 'ProtocolController@index')->name('protocol');
//Noticias
Route::get('/supervisor/noticias', 'NoticeController@index')->name('notice');
//Ficha sintomatológica
Route::get('/supervisor/file', 'InfoController@file')->name('file');
Route::get('/supervisor/file-review/{date?}', 'InfoController@review')->name('file.review');
Route::get('/supervisor/file-filter/{id?}', 'InfoController@filter')->name('file.filter');



Route::get('/lector-qr', 'InfoController@lectorQr')->name('lector.qr');
Route::post('/guardar-qr', 'InfoController@saveQr')->name('store.qr');
Route::get('/historial-qr/{date?}', 'InfoController@reviewQr')->name('qr.review');


});
/*Route::namespace('supervisor')->middleware(['auth','supervisor'])->group(function(){


});*/

Route::get('/home', 'HomeController@panel')->name('home');
Route::get('/admin', 'HomeController@admin')->name('admin');
Route::get('/supervisor', 'HomeController@supervisor')->name('supervisor');
Route::get('/inicio', 'HomeController@usuario')->name('usuario');

//traige
Route::get('/traige-diario-', 'DailyTriageController@index')->name('triage');
Route::post('/guardar-triage','DailyTriageController@store')->name('store.triage');

//reporte de covid
Route::get('/reporte-de-covid-19', 'CaseReportController@index')->name('report.case');
Route::post('/guardar-reporte-','CaseReportController@store')->name('store.report');

//verificación diaria
Route::get('/verificación-diaria', 'DailyCheckController@index')->name('daily.check');
Route::post('/guardar-verificación-diaria','DailyCheckController@store')->name('store.check');


//Protocolos
Route::get('/protocolos', 'ProtocolController@index')->name('protocol');
//Noticias
Route::get('/noticias', 'NoticeController@index')->name('notice');
Route::get('/noticias-del-peru', 'NoticeController@notice')->name('notice.index');
Route::get('/comunicacion-', 'NoticeController@comunication')->name('comunication');



//Ficha sintomatológica
Route::get('/file', 'InfoController@file')->name('file');
Route::post('/guardar-declaración-jurada','InfoController@storeSworn')->name('store.sworn');

//phone
Route::get('/telefonos', 'InfoController@phone')->name('phone');

//qr
Route::get('/mi-qr', 'InfoController@myQr')->name('myqr');


//autenticate
Route::get('/verification-sworn', 'HomeController@verification')->name('verification');

