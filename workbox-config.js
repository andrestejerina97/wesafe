module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{js,png,zip,json,php,css,ico,eot,svg,ttf,woff,woff2,html,jpg,xlsx,txt,xls,config}"
  ],
  "swDest": "public/sw.js"
};