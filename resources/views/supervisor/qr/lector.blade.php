@extends('layouts.views')
@section('css')
<link rel="preload" as="script" href="{asset('js/decoder.js')}}">

<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

    .opa-rec { 
    white-space: nowrap;
    text-align: center;
    font-family: Segoe UI !important;
    font-style: normal;
    font-weight: bold;
    }

   .font-gr{
       font-weight: 750;
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .otro-pregunta-5{
    display: flex;
   }

   .caja-botones{
       width: 100%;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn{
    border-width: 0;
  outline: none;
  border: none;
  border-radius: 6px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
   }


   #loadingMessage {
      text-align: center;
      padding: 40px;
      background-color: #eee;
    }

    #canvas {
    position: relative;
      width: 100%;
      top: 4rem;
      border: solid;
      border-radius: 6%;
      color: rgba(67,32,255,1);
    }

    #output {
      margin-top: 20px;
      background: #eee;
      padding: 10px;
      padding-bottom: 0;
    }

    #output div {
      padding-bottom: 10px;
      word-wrap: break-word;
    }

    #noQRFound {
      text-align: center;
    }

   </style>
@endsection
@section('content')


<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <button onclick="location.href='{{route('supervisor.myqr')}}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff"></i>
        </button>
    </div>
    <div class="col-6 col-sm-6 col-md-6">
        <p class="text-center font-weight-bold" style="font-size: 130%">Lector de QR</p>
    </div>
    <div class="col-3 col-sm-3 col-md-3">
      <button onclick="location.href='{{route('supervisor.qr.review')}}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
    <i class="fa fa-history fa-2x text-right" style="color:#ffffff"></i>
      </button>
  </div>
  </div>
  <div class="row">
    <div class="col-2 col-md-3 col-sm-2" >
    </div>
    <div class="col-8 col-md-3 col-sm-8" >
        <span class="text-center">
        Ubique el código QR dentro del marco y
        evite mover su teléfono
      </span>
</div>
<div class="col-2 col-md-3 col-sm-2" >
</div>
</div>
<div class=" row opa-rec" >
  <div class="col-2 col-md-3 col-sm-3" >
  </div>
  <div class="col-8 col-sm-6 col-md-6 " >
     
    <div id="loadingMessage">🎥</div>
    <div >
       
        <canvas id="canvas"  hidden>
        </canvas>

    </div>
    <div id="output" hidden>
      <div id="outputMessage"></div>
      <div hidden><b></b> <span id="outputData"></span></div>
    </div>
    <div class="mt-4">

    <span id="data_name"></span>
 
    <span id="data_dni"></span>

    </div>

    <hr class="line-sep">
    <section id="section_success">
      <span id="date_success"></span>
      <br>
      <span id="time_success"></span>

    </section>
 
</div>
      
</div>
<div class="col-2 col-md-3 col-sm-3" >
      
</div>
<br>

<div class="col-12 col-md-12 col-lg-12 ">
    <form id="form_qr">
        @csrf
    <input type="hidden" name="id" id="data_id">
  </form>

    <div class="form-group row mb-0">
        <div class="col-md-8 text-center">
            <button id="btn_confirm" type="button" onclick="save_qr()" disabled class="btn btn-login text-light">
               Confirmar
            </button>
        </div>
    </div>
    <br>
    <div class="form-group row mb-4">
        <div class="col-md-8 text-center">
        <button type="button" onclick="location.href='{{route('supervisor.myqr')}}'" class="btn btn-login" style="background-color: #00cca2 !important; color:#ffffff;">
               Regresar
            </button>

        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/jsQR.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

 <script>
    var video = document.createElement("video");
    var canvasElement = document.getElementById("canvas");
    var canvas = canvasElement.getContext("2d");
    var loadingMessage = document.getElementById("loadingMessage");
    var outputContainer = document.getElementById("output");
    var outputMessage = document.getElementById("outputMessage");
    var outputData = document.getElementById("outputData");

    function drawLine(begin, end, color) {
      canvas.beginPath();
      canvas.moveTo(begin.x, begin.y);
      canvas.lineTo(end.x, end.y);
      canvas.lineWidth = 24;
      canvas.strokeStyle = color;
      canvas.stroke();
    }

     // Use facingMode: environment to attemt to get the front camera on phones
     navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } }).then(function(stream) {
      video.srcObject = stream;
      video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
      video.play();
      
      requestAnimationFrame(tick);
    });


    function tick() {
      loadingMessage.innerText = "⌛ cargando video..."
      if (video.readyState === video.HAVE_ENOUGH_DATA) {
        loadingMessage.hidden = true;
        canvasElement.hidden = false;
        outputContainer.hidden = false;
        let arrayDeCadenas
        canvasElement.height = video.videoHeight;
        canvasElement.width = video.videoWidth;
        canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
        var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
        var code = jsQR(imageData.data, imageData.width, imageData.height, {
          inversionAttempts: "dontInvert",
        });
        if (code) {
          drawLine(code.location.topLeftCorner, code.location.topRightCorner, "#FF3B58");
          drawLine(code.location.topRightCorner, code.location.bottomRightCorner, "#FF3B58");
          drawLine(code.location.bottomRightCorner, code.location.bottomLeftCorner, "#FF3B58");
          drawLine(code.location.bottomLeftCorner, code.location.topLeftCorner, "#FF3B58");
          outputMessage.hidden = true;
          outputData.parentElement.hidden = false;
            cadena=code.data+'';
           arrayDeCadenas = cadena.split('@@');
            $("#data_name").text('Nombre: '+arrayDeCadenas[0]);
            $("#data_dni").text('Dni: '+arrayDeCadenas[1]);
            $("#data_id").val(arrayDeCadenas[2]);
            $("#btn_confirm").attr('disabled',false);

         

        } else {
          outputMessage.hidden = false;
          outputData.parentElement.hidden = true;
        }
      }
      
      requestAnimationFrame(tick);
    }

    function save_qr(params) {
       var formdata = new FormData($("#form_qr")[0]);
       $.ajax({
           url         : "{{route('supervisor.store.qr')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text('Fecha de registro: '+data.date);
                 $("#time_success").text('Hora de registro: '+data.time);
                // $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
                swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
               }
             }
       }); 
    }
 </script>
@endsection




