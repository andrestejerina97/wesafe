
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
        <div>
            <div class="Informacion_Personal">
                <span class="font-gr">Información del trabajador</span>
            </div>
        </div>
        <hr class="line-sep">
        @include('admin.partials.data-user')
   
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">1. ¿Quisieras indicar el nombre de la persona a reportar?</span>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4 col-sm-4 col-4">
                <input type="radio"  @if($review->name != '') checked @endif >SI
            </div>
            <div class="col-sm-8 col-md-8 col-sm-4 col-8">
                <div class="input-group input-group-sm mb-1">
                    <input type="text" name="name" id="input_name" value="@if($review->name != '') {{$review->name}} @endif" disabled class="form-control"  aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
        </div>
    </div>
        <div class="row mt-2">
            <div class="col-sm-4 col-md-4 col-4 ">
                
                <input type="radio" disabled  @if($review->name != '')@else checked @endif > NO
            </div>

        </div>

    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">2. ¿Cómo te enteraste de que dicha persona tiene COVID-19?</span>
        </div> 
        <div>
            <textarea name="detail" id="" placeholder="Escribe el detalle..." class="form-control" rows="3">@if($review->detail != ''){{$review->detail}}@endif</textarea>
            
    </div>
</div>
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">3. ¿Qué vínculo tienes con la persona reportada?</span>
            </div> 
    </div>
    <div class="form-check">     
            <div>
            <input class="form-check-input position-static"  checked disabled type="radio" onclick="change_other();" name="link_family" id="blankCheckbox" value="Compañero de trabajo" aria-label="..."><span>@if($review->link_family!=''){{$review->link_family}}@endif</span>
            </div>
    
    </div>
        
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">4. ¿Cuándo fue la última vez que tuvo contacto físico con dicha persona?</span>
            </div> 
    </div>
 <div class="row">
     <div class="col-6 col-sm-6 col-md-6">
        <div class="input-group input-group-sm mb-3">
            <input type="date" class="form-control" name="last_contact" aria-label="Sizing example input" disabled value="@if($review->last_contact!=''){{$review->last_contact}}@endif" aria-describedby="inputGroup-sizing-sm">
        </div>
     </div>
 </div>
    
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">5. ¿Cuánto tiempo tuvo contacto con dicha persona?</span>
            </div> 
    </div>
    <div class="form-check">     
            <div>
                <input class="form-check-input position-static" checked value="" disabled  onclick="change_time()" type="radio" name="time_contact" id="blankCheckbox" value="Menos de 2 horas" aria-label="..."><span>@if($review->time_contact!=''){{$review->time_contact}}@endif</span>
            </div>
         
    </div>
    <br>
    <div class="form-check">     
        <div class="d-flex justify-content-center">
        <button class="btn btn-primary"  onclick="location.href='{{route('supervisor.report.case.review')}}'" type="button">Volver</button>
        </div>
    </div>
		
</div>
</div>
