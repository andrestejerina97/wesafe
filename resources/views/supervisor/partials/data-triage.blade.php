
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">1. ¿Cómo te sientes hoy? Indica si tienes alguno de los siguientes síntomas:</span>
            </div> 
    </div>
        <div class="form-check">     
            @foreach ($options as $option)
         
            @if($option->name=='Otro')
            <div class="otro-pregunta-5">
                <input class="form-check-input position-static" id="symptom" disabled type="checkbox" name="symptom[]" @if($reviews->symptom_other != '')checked @endif   aria-label="..."><span>Otro</span>
                <div class="input-group input-group-sm mb-1 ml-4">
                    <input type="text" 
                value="@if($reviews->symptom_other != ''){{$reviews->symptom_other}}@endif " 
                    placeholder="Indique otro síntoma.." class="form-control" id="input_symptom"  name="input_symptom" disabled aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
            @else
            <div>
                <input class="form-check-input position-static" disabled name="symptom[]" type="checkbox" id="symptom" checked value="{{$option->id}}" aria-label="..."><span>{{$option->name}}</span>
                </div>
            @endif 
            @endforeach
           
        </div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
        <span class="font-gr"> 2. Desde su última evaluación ¿Ha tenido contacto con una o más personas con caso confirmado de COVID-19?</span>
        
        <div class="d-flex ">
            <div class="col ">
                <input type="radio" disabled  @if($reviews->contact =='Si') checked @endif>SI
            </div>
            <div class=" col">
                
                <input  disabled type="radio" @if($reviews->contact !='Si') checked @endif >NO 
            </div>
        </div>
   
    </div>
		