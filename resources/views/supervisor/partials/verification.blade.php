<div class="opa-rec" >
  <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
            <div class="text-center">
                <img src="{{asset('img/logo.png')}}" style="max-height: 60%; max-width:60%" alt="">
               </div>
            <div class="col-12 col-md-12 col-lg-12 col-sm-12 ">
                <div class="row text-center">
                    <span class="" style="color: azure">Por favor,confirma tu identidad para continuar</span>
                </div>
                      
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Usuario</label>
    
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
    
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                   
                                   
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right">Nro de documento</label>
                                <div class="col-md-6">
                                    <input id="ci" type="text" class="form-control @error('ci') is-invalid @enderror" name="ci" required>
                                
                                </div>
                            </div>
                           
    
                            <div class="form-group row mb-2">
                                <div class="col-md-8 text-center">
                                    <button type="submit"  class="btn btn-primary btn-login">
                                       Confirmar
                                    </button>
    
                                   
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 text-center">
                                    <button type="button" onclick="back();" class="btn btn-login" style="background-color: #00cca2 !important; color:#ffffff;">
                                       Regresar
                                    </button>
      
                                </div>
                            </div>
                      
            </div>
        </div>
    </div>