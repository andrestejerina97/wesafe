@extends('layouts.views')
@section('css')
<style>
	
	#Rectngulo_49 {
		fill: rgba(0,204,170,0.498);
	}
	.Rectngulo_49 {
		position: absolute;
		overflow: visible;
		width: 200px;
        height: 200px;
        left: 20%;
        top: 18%;
}
	}
    
	#Rect {
		fill: rgba(0,204,170,0.498);
	}
	.Rectngulo_50 {
		position: absolute;
		overflow: visible;
		width: 200px;
        height: 200px;
        left: 20%;
        top: 60%;
        fill: rgba(0,204,170,0.498)

	}
	#Icon_awesome-clipboard-list {
		fill: rgba(255,255,255,1);
	}
	.Icon_awesome-clipboard-list {
		overflow: visible;
		position: relative;
        width: 40%;
        height: 40%;
		margin-top: 0;

		transform: matrix(1,0,0,1,0,0);
	}

	#Icon_material-find-in-page {
		fill: rgba(255,255,255,1);
	}
	.Icon_material-find-in-page {
		overflow: visible;
		position: relative;
		width: 40%;
        height: 40%;
		transform: matrix(1,0,0,1,0,0);
	}
	.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 90%;
		height: 5%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn{
	    white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 130%;
		color: rgba(255,255,255,1);
		background-color: rgba(0,170,204,0.498);
		width: 65%;
		height: 95%;
   }
   </style>

@endsection
@section('content')
<div class="row mb-4 mt-4 bg-secondary text-white ">
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
        <button onclick="location.href='{{route('home') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
	<div class="col-lg-10 col-md-10 col-sm-10 col-10 mr-2">
        <p class="text-center font-weight-bold" style="font-size: 120%">Reporte de COVID-19</p>
	</div>
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
	</div>
  </div>
  <div class="row ">

	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
	</div>

	<div class="col-10 col-sm-10 text-center">
	<button class="btn "  onclick="location.href='{{route('report.case')}}'">
		<div class="mb-2">
			<img style="max-width: 65%;" src="{{asset('img/logo_reporte.png')}}" alt="">
		</div>
            <p class="text-center">Reporte de<br>COVID-19</p>
	</button>

	</div>
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
	</div>

	
  </div>
  <div class="row mt-4">
	  
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
	</div>

	<div class="col-10 col-sm-10 text-center">

		<button class="btn" onclick="location.href='{{route('supervisor.report.case.review')}}'" >
			<svg class="Icon_material-find-in-page" viewBox="36 0 107.619 226.867">
				<path id="Icon_material-find-in-page" d="M 178.8616180419922 193.0397338867188 L 178.8616180419922 67.82310485839844 L 114.0385131835938 3 L 27.60770225524902 3 C 15.72346496582031 3 6.108040332794189 12.72346496582031 6.108040332794189 24.60770034790039 L 6 197.4693298339844 C 6 209.3535614013672 15.61542797088623 219.0770263671875 27.49966239929199 219.0770263671875 L 157.25390625 219.0770263671875 C 162.1156463623047 219.0770263671875 166.4371795654297 217.4564666748047 170.1104888916016 214.7554931640625 L 122.2494354248047 166.8944244384766 C 113.6063537597656 172.5124359130859 103.4507293701172 175.8616333007813 92.43080902099609 175.8616333007813 C 62.6121826171875 175.8616333007813 38.41155242919922 151.6610107421875 38.41155242919922 121.8423767089844 C 38.41155242919922 92.02375030517578 62.61217498779297 67.8231201171875 92.43080902099609 67.8231201171875 C 122.2494354248047 67.8231201171875 146.4500579833984 92.02374267578125 146.4500579833984 121.8423767089844 C 146.4500579833984 132.8623199462891 143.1008605957031 143.0179443359375 137.4828491210938 151.5529632568359 L 178.8616180419922 193.0397338867188 Z M 60.01925659179688 121.8423614501953 C 60.01925659179688 139.7767486572266 74.49641418457031 154.2539215087891 92.43080902099609 154.2539215087891 C 110.3651962280273 154.2539215087891 124.8423614501953 139.7767486572266 124.8423614501953 121.8423614501953 C 124.8423614501953 103.9079666137695 110.3651962280273 89.43080902099609 92.43080902099609 89.43080902099609 C 74.49641418457031 89.43080902099609 60.01925659179688 103.9079666137695 60.01925659179688 121.8423614501953 Z">
				</path>
			</svg>
				<p>Revisión de<br/>registros</p>
		</button>
	</div>
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
	</div>
  </div>


@endsection


