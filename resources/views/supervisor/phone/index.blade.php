@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .otro-pregunta-5{
    display: flex;
   }

   .caja-botones{
       width: 100%;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn{
    border-width: 0;
  outline: none;
  border-radius: 10px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
   }

   </style>
@endsection
@section('content')

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <a href="{{route('usuario')}}">
            <svg  class="Icon_awesome-arrow-left" viewBox="-9.004 -0 10 10" >
                <path id="Icon_awesome-arrow-left"  d="M 41.50252532958984 68.31076812744141 L 37.92483139038086 71.88845062255859 C 36.40995788574219 73.40333557128906 33.96036529541016 73.40333557128906 32.46160125732422 71.88845062255859 L 1.13264262676239 40.57561111450195 C -0.382235050201416 39.06072998046875 -0.382235050201416 36.61114883422852 1.13264262676239 35.11238479614258 L 32.46160125732422 3.78342342376709 C 33.97648239135742 2.268545866012573 36.42607116699219 2.268545866012573 37.92483139038086 3.78342342376709 L 41.50252532958984 7.361112594604492 C 43.03351593017578 8.892107009887695 43.00128173828125 11.39004421234131 41.43805694580078 12.88880634307861 L 22.01861572265625 31.3896541595459 L 68.33519744873047 31.3896541595459 C 70.47857666015625 31.3896541595459 72.20296478271484 33.11403274536133 72.20296478271484 35.25742340087891 L 72.20296478271484 40.41445541381836 C 72.20296478271484 42.55784606933594 70.47857666015625 44.28223037719727 68.33519744873047 44.28223037719727 L 22.01861572265625 44.28223037719727 L 41.43805694580078 62.7830696105957 C 43.01739501953125 64.28183746337891 43.04962921142578 66.77977752685547 41.50252532958984 68.31076812744141 Z">
                </path>
            </svg>
        </a>
    </div>
    <div class="col-7 col-xs-7 col-md-7">
        <p class="text-center font-weight-bold">Teléfonos de emergencia</p>
    </div>
</div>

<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
       @foreach ($phones as $phone)
    <a href="tel:{{$phone->number}}">
       <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
       <button type="button" class="btn btn-success d-flex justify-content-around"> <p>{{$phone->description}}</p> <p>{{$phone->number}}</p></button> 
        </div>
      </a>
       @endforeach
    </div>
		

</div>

@endsection


