@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .card{
       border-radius: 10% !important;
       background-color: rgba(204,204,204,0.8) !important;
   }
   .title{
    font-size: 130%;
    margin-left: 20%;
}
   
@media (max-width: 350px) {
 
    .title{
    font-size: 120%;
        


}
}
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-2 col-sm-2 col-md-1">
        <button onclick="location.href='{{route('supervisor.triage') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
      <i class="fa fa-arrow-left fa-2x" aria-hidden="true" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-8 col-sm-8 col-md-8 ">
        <p class="text-center font-weight-bold title">Triajes registrados</p>
    </div>
    <div class="col-2 col-sm-2 col-md-1">
    </div>
  </div>
<!--Fin Encabezado inicial -->
<form action="{{route('supervisor.triage')}}" method="POST" onsubmit="save(event)" id="form_check">
<div class="ml-3 d-flex">
    <div class="input-group">
    <label for="">Indique la fecha</label>
    <input type="date" name="date" class="form-control col-6" value="{{$date}}" onchange="select_date(this)">
    </div>
</div>
<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
        <hr class="line-sep">
        @foreach ($reviews as $review)
    <a href="{{route('supervisor.triage.filter',$review->id)}}" style="color: #000000; font-size: 120%;">
        <div>
            <div>
                <span>Registrado por:</span>
            <span>{{$review->name}}</span>
            </div>
        <div>
            <span>Fecha:</span>
            <span>{{date('Y-m-d', strtotime($review->created_at))}}</span>
        </div>
        <div>
            <span>Hora:</span>
            <span>{{date('H:i:s', strtotime($review->created_at))}}</span>
        </div>
        <hr class="line-sep">
        </div>
    </a>
        @endforeach
            @csrf
        <div id="option"></div>
        <br>
        <div class="mx-auto">
            <ul class="pagination ">
            <li ><a class="last_page btn " href="javascript:;">Página {{$reviews->currentPage()}} de {{$reviews->lastPage()}}</a></li>
                <li ><a class="last_page btn btn-color" style="background-color: rgba(67,32,255,1);color:#ffffff;border-radius: 10px;" href="{{$reviews->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
                <li><a class="next_page btn btn-color" style="background-color: rgba(67,32,255,1);color:#ffffff; border-radius: 10px;" href="{{$reviews->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
              </ul>
        </div>
    </div>
		
</div>
</section>
<section id="section_verification" style="display: none">
    @include('public.partials.confirm')

</section>
</form>

<section id="section_success" style="display: none">
    @include('public.partials.success')
</section>
@endsection
@section('scripts')
<script>
    function select_date(a) {
       url='{{route('supervisor.triage.review',["date"=> 0])}}';
       url=url.replace('0',$(a).val());
       location.href=url;
        
    }
</script>
@endsection


