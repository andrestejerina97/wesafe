@extends('layouts.admin')
@section('scripts')

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Proyectos") {
                $(this).parent().addClass("active");
            }
     });
});
</script>
    
@endsection
@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="table-responsive">
    <table class="table ">
        <thead class=" ml-2">
        <tr>
        <th>Fecha de ingreso</th>
        <th>Nombre completo</th>
        <th >Descripción del proyecto</th>
        <th>Dirección</th>
        <th></th>

    </tr>
        </thead>
    <tbody>
        @foreach($proyects as $proyect)
        <tr>
        <td>{{$proyect->created_at}}</td>
        <td>{{$proyect->name_proyect}}</td>
        <td>{{$proyect->details}}</td>
        <td>{{$proyect->city_province}}</td>
        <td>
            <a href="{{route('admin.edit.proyect',['id'=>$proyect->id])}}" class="btn-admin">Ir al proyecto</a>
        </td>
        </tr>
        @endforeach

    </tbody>
    </table>
    </div>
</div>    
</div> 

@endsection