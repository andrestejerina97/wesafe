@extends('layouts.admin')
@section('scripts')

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Oportunidades") {
                $(this).parent().addClass("active");
            }
     });
});
</script>
    
@endsection
@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="table-responsive">
    <table class="table ">
        <thead class=" ml-2">
        <tr>
        <th>Fecha de ingreso</th>
        <th>Nombre completo</th>
        <th >Descripción de oportunidad</th>
        <th>Dirección</th>
        <th></th>

    </tr>
        </thead>
    <tbody>
        @foreach($oportunities as $oportunity)
        <tr>
        <td>{{$oportunity->created_at}}</td>
        <td>{{$oportunity->name_oportunity}}</td>
        <td>{{$oportunity->details}}</td>
        <td>{{$oportunity->city_province}}</td>
        <td>
            <a href="{{route('admin.edit.oportunity',['id'=>$oportunity->id])}}" class="btn-admin">Ir a oportunidad</a>
        </td>
        </tr>
        @endforeach

    </tbody>
    </table>
    </div>
</div>    
</div> 

@endsection