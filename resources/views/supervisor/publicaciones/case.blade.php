@extends('layouts.admin')

@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
    <div class="ml-4 col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center ">
        <div class="panel panel-default">
         
        <div class="table-responsive">
    <table class="table table-hover">

        <thead class=" ml-4">

          <tr style="border: 1px solid transparent">
            <td>
            <form id="form_1"  action="{{route("admin.search.case")}}" method="POST">
              @csrf
              <input type="hidden" id="filter" name="filter" value="code">

                <div class="input-group col-lg-9 col-md-8 ">
                  <a href="javascript:{}" onclick="document.getElementById('form_1').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input placeholder="Buscar por N°" type="text" class="form-control" id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>
            </td>
            <td>
              <form id="form_2"  action="{{route("admin.search.case")}}" method="POST">
                @csrf
                <input type="hidden" id="filter" name="filter" value="name_case">
                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_2').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input type="text" placeholder="Buscar por nombre.." class="form-control" id="search" name="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>

            </td>
            <td>
              <form id="form_3" action="{{route("admin.search.case")}}" method="POST">
                @csrf
                <input type="hidden" id="filter" name="filter" value="city_province">

                <div class="input-group col-lg-9 col-md-8">
                  <a href="javascript:{}" onclick="document.getElementById('form_3').submit();" class="input-group-addon"><i class="fa fa-search"></i></a>
                  <input placeholder="Buscar por ubicación" type="text" class="form-control" name="search" id="search" aria-describedby="inputGroupSuccess1Status">
                </div>
              </form>
            </td> 
               <td></td>
               <td></td>
          </tr>
        <tr>
        <th class="text-left">Nro Caso</th>
        <th class="text-left">Nombre</th>
        <th class="text-left" >Ubicación</th>
        <th class="text-left">Progreso</th>
        <th class="text-center" style="width: 18%"><a href="{{route('admin.edit.case')}}" class="btn-admin">+ Nuevo caso</a>
        </th>

    </tr>
        </thead>
    <div class="panel-body">

    <tbody>
        @foreach($publications as $publication)
        <tr>
        <td class="text-left">{{$publication->code}}</td>
        <td class="text-left">{{$publication->name_case}}</td>
        <td class="text-left">{{$publication->city_province}}</td>
        <td class="text-left">{{$publication->progress}}</td>
        <td>
            <a href="#" class="btn-table"><i class="fa fa-history"></i></a>
            <a href="{{route('admin.edit.case',['id'=>$publication->id])}}" class="btn-table"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <a href="javascript:;" data-id_case={{$publication->id}} onclick="eliminar(this);" class="btn-table"><i class="fa fa-trash" aria-hidden="true"></i></a>

        </td>
        </tr>
        @endforeach

    </tbody>
</div>
    </table>
    </div>
</div>
</div>    
</div> 

@endsection

@section('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $(function(){
        $('.nav-text').each(function () {
            if ($(this).text()=="Casos") {
                $(this).parent().addClass("active");
            }
     });
});
    function eliminar(a) {
          swal({
              text: 'Seguro que deseas eliminar el caso".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
                  let id_case=$(a).data("id_case");
               let url="{{route('admin.delete.case','id')}}"
               url=url.replace("id",id_case);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Caso eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("cases.index")}}';


                      });
                          $("#btn_delete").data("active",-1);
                      $("#btn_delete").data("id_case",-1);

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }    
    </script>    
@endsection