@extends('layouts.views')
@section('css')
<style>
	
    #Rectangle_5 {
		fill: rgba(114,84,0,0.502);
	}
	.Rectangle_5 {
		position: absolute;
		overflow: visible;
		left: 17%;
        top: 18%;
	}
	#Rectangle_5_Copy {
		fill: rgba(89,15,15,0.651);
	}
	.Rectangle_5_Copy {
		filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.502));
		position: absolute;
		overflow: visible;
		left: 17%;
        top: 58%;
	}
	#Trazado_10 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_10 {
		overflow: visible;
		position: absolute;
        width: 24%;
        height: 225.603px;
        left: 33%;
        top: 52%;
		transform: matrix(1,0,0,1,0,0);
	}
	#Grupo_23 {
		position: absolute;
        width: 155.512px;
    height: 173.097px;
    left: 20%;
    top: 20%;
		overflow: visible;
	}
	#Trazado_11 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_11 {
		overflow: visible;
		position: absolute;
		width: 24%;
    height: 124.13px;
    left: 28%;
    top: -3%;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_12 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_12 {
		overflow: visible;
		position: absolute;
		width: 57%;
    height: 123.749px;
    left: 39%;
    top: -3%;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_13 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_13 {
		overflow: visible;
		position: absolute;
		width: 14.709px;
    height: 10px;
    left: 36%;
    top: 40.695px;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_14 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_14 {
		overflow: visible;
		position: absolute;
		width: 10px;
    height: 31.274px;
    left: 107.886px;
    top: 94.824px;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_15 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_15 {
		overflow: visible;
		position: absolute;
		width: 14.844px;
    height: 58.629px;
    left: 108.049px;
    top: -11px;
		transform: matrix(1,0,0,1,0,0);
	}
	
	
	#Comunicacin_Interna {
		left: 9%;
        top: 39%;
		position: absolute;
		overflow: visible;
		width: 291px;
		white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 25px;
		color: rgba(255,255,255,1);
	}
	#Cifras_en_el_Per {
		left: 19%;
        top: 79%;
		position: absolute;
		overflow: visible;
		width: 227px;
		white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 25px;
		color: rgba(255,255,255,1);
	}

	.bot-1{
		height: 37%;
		width: 94%;
		background: none;
		left: 31%;
		top: 28%;
		border: none;
	}
	.bot-2{
		height: 39%;
		width: 55%;
		border: none;
		background: none;
	}
    .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   
    .otro-pregunta-5{
    display: flex;
   }

</style>

@endsection
@section('content')

<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-sm-3 col-md-3">
        <a href="{{route('usuario')}}">
            <svg  class="Icon_awesome-arrow-left" viewBox="-9.004 -0 10 10" >
                <path id="Icon_awesome-arrow-left"  d="M 41.50252532958984 68.31076812744141 L 37.92483139038086 71.88845062255859 C 36.40995788574219 73.40333557128906 33.96036529541016 73.40333557128906 32.46160125732422 71.88845062255859 L 1.13264262676239 40.57561111450195 C -0.382235050201416 39.06072998046875 -0.382235050201416 36.61114883422852 1.13264262676239 35.11238479614258 L 32.46160125732422 3.78342342376709 C 33.97648239135742 2.268545866012573 36.42607116699219 2.268545866012573 37.92483139038086 3.78342342376709 L 41.50252532958984 7.361112594604492 C 43.03351593017578 8.892107009887695 43.00128173828125 11.39004421234131 41.43805694580078 12.88880634307861 L 22.01861572265625 31.3896541595459 L 68.33519744873047 31.3896541595459 C 70.47857666015625 31.3896541595459 72.20296478271484 33.11403274536133 72.20296478271484 35.25742340087891 L 72.20296478271484 40.41445541381836 C 72.20296478271484 42.55784606933594 70.47857666015625 44.28223037719727 68.33519744873047 44.28223037719727 L 22.01861572265625 44.28223037719727 L 41.43805694580078 62.7830696105957 C 43.01739501953125 64.28183746337891 43.04962921142578 66.77977752685547 41.50252532958984 68.31076812744141 Z">
                </path>
            </svg>
        </a>
    </div>
    <div class="col-7 col-sm-7 col-md-7">
        <p class="text-center font-weight-bold">Noticias</p>
    </div>
  </div>
  <!--Fin Encabezado inicial -->
  
<button  class="bot-1"><div>
	
		<div id="Grupo_23">
			<svg class="Trazado_11" viewBox="4514.12 -214.112 53.387 114.13">
				<path id="Trazado_11" d="M 4514.11962890625 -99.98155975341797 L 4514.11962890625 -187.4181213378906 C 4514.11962890625 -187.4181213378906 4516.0048828125 -214.1120300292969 4540.8134765625 -214.1120300292969 C 4565.62255859375 -214.1120300292969 4567.5068359375 -187.4181213378906 4567.5068359375 -187.4181213378906 L 4567.5068359375 -99.98155975341797 L 4514.11962890625 -99.98155975341797 Z">
				</path>
			</svg>
			<svg class="Trazado_12" viewBox="4529.832 -213.91 125.886 113.749">
				<path id="Trazado_12" d="M 4553.59228515625 -100.1606063842773 L 4655.7177734375 -100.1606063842773 L 4655.7177734375 -187.9424438476563 C 4655.7177734375 -187.9424438476563 4651.7958984375 -213.9098510742188 4629.75048828125 -213.9098510742188 C 4607.70556640625 -213.9098510742188 4529.83154296875 -213.9098510742188 4529.83154296875 -213.9098510742188">
				</path>
			</svg>
			<svg class="Trazado_13" viewBox="4522.65 -191.307 23.709 10">
				<path id="Trazado_13" d="M 4522.64990234375 -191.3065948486328 L 4546.359375 -191.3065948486328">
				</path>
			</svg>
			<svg class="Trazado_14" viewBox="4559.668 -153.584 10 31.274">
				<path id="Trazado_14" d="M 4559.66845703125 -153.5839996337891 L 4559.66845703125 -122.3102874755859">
				</path>
			</svg>
			<svg class="Trazado_15" viewBox="4566.649 -228.799 20.844 53.629">
				<path id="Trazado_15" d="M 4566.6494140625 -175.1700592041016 L 4566.6494140625 -228.7987823486328 L 4587.4931640625 -228.7987823486328">
				</path>
			</svg>
		</div>
		<div id="Comunicacin_Interna">
			<span>Comunicación<br/>Interna</span>
		</div>
	</div>
	
</button>

<button  class="bot-2">
		<div>
			<svg class="Rectangle_5_Copy">
				<rect id="Rectangle_5_Copy" rx="0" ry="0" x="0" y="0" width="225" height="225">
				</rect>
			</svg>
			<svg class="Trazado_10" viewBox="4374.222 221.818 140.208 206.01">
				<path id="Trazado_10" d="M 4374.2216796875 281.1694946289063 L 4374.2216796875 271.7076416015625 L 4381.96337890625 263.5360717773438 L 4391.4248046875 271.7076416015625 L 4400.45751953125 281.1694946289063 L 4407.33837890625 271.7076416015625 L 4407.33837890625 263.5360717773438 L 4415.509765625 255.3644866943359 L 4424.97216796875 255.3644866943359 L 4432.712890625 247.1929168701172 L 4440.88525390625 231.2798309326172 L 4440.88525390625 221.8179626464844 L 4458.08837890625 239.8815155029297 L 4466.259765625 247.1929168701172 L 4474.0009765625 247.1929168701172 L 4481.7431640625 247.1929168701172 L 4489.0537109375 247.1929168701172 L 4497.65625 255.3644866943359 L 4497.65625 271.7076416015625 L 4489.0537109375 263.5360717773438 L 4481.7431640625 271.7076416015625 L 4474.0009765625 273.4280090332031 L 4466.259765625 278.158935546875 L 4466.259765625 295.3622741699219 L 4458.08837890625 303.9638977050781 L 4458.08837890625 312.5656127929688 L 4466.259765625 321.167236328125 L 4474.0009765625 329.3388366699219 L 4481.7431640625 336.22021484375 L 4489.0537109375 336.22021484375 L 4489.0537109375 329.3388366699219 L 4505.3974609375 344.8218688964844 L 4510.2509765625 354.0663452148438 L 4514.42919921875 362.0252075195313 L 4514.42919921875 377.9382629394531 L 4507.11767578125 394.7115478515625 L 4514.42919921875 403.7433166503906 L 4507.11767578125 419.6563720703125 L 4497.65625 427.8279724121094 L 4490.77490234375 419.6563720703125 L 4481.7431640625 411.9148864746094 L 4466.259765625 403.7433166503906 L 4458.08837890625 403.7433166503906 L 4432.712890625 379.6586303710938 L 4424.97216796875 353.8536376953125 L 4415.509765625 344.8218688964844 L 4407.33837890625 329.3388366699219 L 4400.45751953125 312.5656127929688 L 4391.4248046875 295.3622741699219 L 4383.6845703125 287.6207885742188 L 4374.2216796875 281.1694946289063 Z">
				</path>
			</svg>
			
			<div id="Cifras_en_el_Per">
				<span>Cifras en el<br/>Perú</span>
			</div>
		</div>
</button>



@endsection


