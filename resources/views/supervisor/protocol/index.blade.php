@extends('layouts.views')
@section('css')
<style>
    


   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px; 
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
  
   </style>
@endsection
@section('content')
<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <button onclick="location.href='{{route('home') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-8 col-xs-8 col-md-8">
        <p class="text-center font-weight-bold" style="font-size: 130%">Protocolos o procedimientos</p>
    </div>
  </div>

<div class="opa-rec" >
   <div>
        <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
                <div class="accordion" id="accordionExample">
                    <div class="card">

            @foreach ($protocols as $protocol)
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button onclick="change_icon(this);" data-active=0 class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse_{{$protocol->id}}
                            " aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-folder"></i>
                            {{$protocol->name}}
                        </button>
                    </h2>
                    </div>
                    <div id="collapse_{{$protocol->id}}" class="collapse hidden" aria-labelledby="collapse_{{$protocol->id}}" data-parent="#accordionExample">
                    <div class="card-body d-flex-column justify-content-around">
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de ambientes.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Inspección de materiales de higiene.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Lavado de manos.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Desinfección de SSHH.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de vehículos.</button>
                    </div>
                    </div>
               
                @endforeach

            </div>
                </div>
            </div>
    </div>	

</div>


@endsection
@section('scripts')
<script>
function change_icon(a) {
//$(a).child('<i>').removeClass('fa');
if ($(a).data('active') < 1) {

$( ".btn-block" ).each(function( index ) {
    if ($(this).data('active') > 0) {
    $(this).children('.fa').removeClass('fa-folder-open-o');
    $(this).children('.fa').addClass('fa-folder');
    $(this).data('active',0);

    }
});
//$(a).children('.fa').removeClass('fa-folder')
//$(a).child('<i>').removeClass('fa');
$(a).children('.fa').addClass('fa-folder-open-o')
$(a).data('active',1);
}else{

    $( ".btn-block" ).each(function( index ) {
    if ($(this).data('active') > 0) {
    $(this).children('.fa').removeClass('fa-folder-open-o');
    $(this).children('.fa').addClass('fa-folder');
    $(this).data('active',0);

    }
    });
//$(a).children('.fa').removeClass('fa-folder-open-o')
//$(a).child('<i>').removeClass('fa');
$(a).children('.fa').addClass('fa-folder')
$(a).data('active',0);

}

}    
</script>    
@endsection

