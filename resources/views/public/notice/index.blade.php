@extends('layouts.views')
@section('css')
<style>
	
    #Rectangle_5 {
		fill: rgba(114,84,0,0.502);
	}
	.Rectangle_5 {
		position: absolute;
		overflow: visible;
		left: 17%;
        top: 18%;
	}
	#Rectangle_5_Copy {
		fill: rgba(89,15,15,0.651);
	}
	.Rectangle_5_Copy {
		filter: drop-shadow(0px 3px 6px rgba(0, 0, 0, 0.502));
		position: absolute;
		overflow: visible;
		left: 17%;
        top: 58%;
	}
	#Grupo_23{
		position: relative;
		width: 90%;
		height: 100%;
		color: azure !important;
		
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
	#Trazado_11 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_11 {
		overflow: visible;
		position: absolute;
		width: 24%;
    height: 124.13px;
    left: 28%;
    top: -3%;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_12 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_12 {
		overflow: visible;
		position: absolute;
		width: 57%;
    height: 123.749px;
    left: 39%;
    top: -3%;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_13 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_13 {
		overflow: visible;
		position: absolute;
		width: 14.709px;
    height: 10px;
    left: 36%;
    top: 40.695px;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_14 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_14 {
	overflow: visible;
	position: absolute;
	width: 10px;
    height: 31.274px;
    left: 107.886px;
    top: 94.824px;
		transform: matrix(1,0,0,1,0,0);
	}
	#Trazado_15 {
		fill: transparent;
		stroke: rgba(255,255,255,1);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Trazado_15 {
		overflow: visible;
		position: absolute;
		width: 14.844px;
    height: 58.629px;
    left: 108.049px;
    top: -11px;
		transform: matrix(1,0,0,1,0,0);
	}
	
	#Trazado_10{
		fill: rgba(255,255,255,1);
	}
	.Trazado_10{
		overflow: visible;
		position: relative;
		width: 80%;
        height: 80%;
		transform: matrix(1,0,0,1,0,0);
	}

	.bot-1{
		height: 37%;
		width: 94%;
		background: none;
		left: 31%;
		top: 28%;
		border: none;
	}
	.bot-2{
		height: 39%;
		width: 55%;
		border: none;
		background: none;
	}
	.btn{
	    white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: no	rmal;
		font-weight: bold;
		color: rgba(255,255,255,1);
		background-color: rgba(114,84,0,0.502);
		width: 65%;
		height: 100%;
   }
   .btn2{
	    white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 120%;
		color: rgba(255,255,255,1);
		background-color: #590f0fa6;
		width: 65%;
		height: 100%;
		margin-top: 15%;
		border:none;
   }
   .comunicacion{
	   position: relative;
	   top: 120px !important;
	   left: 15px;
   }
</style>

@endsection
@section('content')


<div class="row mb-4 mt-4 bg-secondary">
	<div class="col-lg-2 col-md-2 col-sm-2 col-2">
        <button onclick="
		$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');
		  location.href='{{route('home') }}'" 
		  class=" btn-secondary" style="border:none; " type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x"  style="color:#ffffff;"></i>
        </button>
    </div>
	<div class="col-lg-8 col-md-8 col-sm-8 col-8 ">
		<h4 class="text-center font-weight-bold text-white mb-0">Noticias</h4>
	</div>
	<div class="col-lg-2 col-md-2 col-sm-2 col-2">
    </div>
  </div>
 <div class="row">
	<div class="col-lg-1 col-md-1 col-sm-1 col-1 ">
	</div>
	
	<div class="col-10 col-sm-10 text-center">
	<button onclick="$('#loader').removeClass('disabled');
	$('#menu_home').addClass('loading');location.href='{{route('comunication')}}'"  class="btn">
				<div class="mb-2">
                    <img style="max-width: 65%;" src="{{asset('img/comunicacion.png')}}" alt="">
                </div>
				<span class="" >	Comunicación<br/>Interna	
				</span>
	</button>
	</div>
	
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 ">
	</div>
 </div>


  <div class="row mt-4">
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 ">
	</div>
	
	<div class="col-10 col-sm-10 text-center">
	
<button onclick="$('#loader').removeClass('disabled');
 $('#menu_home').addClass('loading');location.href='{{route('notice.index')}}'"  class="btn" style="background-color: #590f0fa6 ">
             <div class="mb-2">
				<img style="max-width: 65%;" src="{{asset('img/peru.png')}}" alt="">
			</div>
		Cifras <br>en el Perú
</button>
	</div>
	
	<div class="col-lg-1 col-md-1 col-xs-1 col-1 ">
	</div>
 </div>






@endsection


