@extends('layouts.views')
@section('css')
<style>
	

</style>

@endsection
@section('content')

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-2 col-sm-2 col-md-2">
        <a onclick="$('#loader').removeClass('disabled');
        $('#menu_home').addClass('loading');location.href='{{route('notice') }}'" class="btn btn-secondary" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x "  style="color:#ffffff;"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8">
   		<h4 class="text-center font-weight-bold text-white mb-0">Comunicación interna</h4>
    </div>
    <div class="col-2 col-sm-2 col-md-2">

    </div>
  </div>
 <div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-12 ">
        <div class="card" style="height:100%; opacity:0.9; border: none;">
            <div class="card-header">
                <h5 class="text-center font-weight-bold">
                    La empresa obtiene certificado ISO 9001
                </h5>
            </div>
            <div class="card-body">
                <div class="mb-2">
                    <img style="max-width: 65%;" src="{{asset('storage/comunication/iso.png')}}" alt="">
                </div>
                <h5>El día 08/08/2020 obtuvimos la certificación ISO 9001 para el proceso de ventas corporativas</h5>
            </div>
        </div>
	</div>
 </div>

@endsection


