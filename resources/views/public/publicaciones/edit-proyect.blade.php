@extends('layouts.admin')
@section('css')

<link href="{{asset('css/slick.css')}}" rel="stylesheet"> 
// Add the new slick-theme.css if you want the default styling
<link href="{{asset('css/slick-theme.css')}}" rel="stylesheet"> 
@endsection
@section('content')
     <!-- Side navigation -->


<!-- Page content -->
<div class="main">
  <div class="row">
    @foreach ($proyects as $proyect)

      <div class="col-lg-7 col-xs-7">
        @include('admin.partials.form-proyect-left')

      </div>
      <div class="col-lg-5 col-xs-5">
        @include('admin.partials.form-proyect-rigth')
      </div>
      @endforeach

  </div>
</div> 

@endsection
@section('scripts')
           
    <!-- carousel logos empresas -->   	
    <script src="{{asset('js/slick.min.js')}}"></script>
    <!-- end - carousel logos empresas -->  
      <script>
   
$('.carousel-proyects').slick({
  centerMode: true,
  lazyLoad: 'ondemand',
  centerPadding: '0px',
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});

      
      </script>

@endsection