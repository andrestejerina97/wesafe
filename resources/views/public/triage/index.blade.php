@extends('layouts.views')
@section('css')
    <style>
    
     .line-sep{
        border-style: solid;
        background-color: blue;
        height: 2px;
     }

    .opa-rec {
        opacity: 0.9;
        color: black;
        margin: 15px;
       
    }

    .font-gr{
        font-weight: 750;
    }
	
    .Informacion_Personal {
		filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
		overflow: visible;
		white-space: nowrap;
		text-align: left;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 18px;
		color: rgba(0,0,0,1);
	}

    .ID1_Cmo_te_sientes_hoy_Indica_ {
		filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
		text-align: left;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 13px;
		color: rgba(0,0,0,1);
	}

    .rect-ps1{
        position: relative;
        top: 40px; left: 40px;
    }

    .btn.btn-primary {
        background: #007eea !important;
        border-color: #007eea !important;
    }
    .otro-pregunta-5{
    display: flex;
   }
   .btn-success{
    width: 100px;
    height: 30px;
    filter: drop-shadow(5px 5px 5px rgba(0, 102, 81, 1));
    font-family: Segoe UI;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    border: none;
    background-color: rgba(0,204,162,1) !important;

}
 .active{
    background-color: rgb(7, 131, 106) !important;
}
#Enviar {
    white-space: nowrap;
    text-align: center;
    font-family: SegoeUI;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    color: rgba(255,255,255,1);
}
.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   
    .otro-pregunta-5{
    display: flex;
   }
    </style>
@endsection
@section('content')
<!--Encabezado inicial -->
<section  id="section_check" >

<form  method="POST" onsubmit="save(event)" id="form_check">
    @csrf
<div class="row mb-4 mt-4 bg-secondary">
    <div class="col-2 col-sm-2 col-md-2">
        <a href="{{route('home')}}" onclick="	$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');">
            <i class="fa fa-arrow-left fa-2x" style="color:#ffffff; margin-top:7%"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center align-self-center">
        <h5 class="text-center font-weight-bold text-white mb-0">Triaje diario</h5>
    </div>
    <div class="col-2 col-sm-2 col-md-2">
    </div>
  </div>
  <!--Fin Encabezado inicial -->
  
<div class="opa-rec" >
    <div  class="p-3 card text-black" style="background-color: rgba(204,204,204,0.8)" id="mb-inferior">
     
        @include('public.partials.data-triage')
</div>
</section>
<br><br>
<section id="section_success" class="mt-4" style="display: none">
    @include('public.partials.success')
</section>
<section  id="section_verification" class="mt-4" style="display: none">
    @include('public.partials.confirm')
</section>
<input type="hidden" value="No" id="contact" name="contact">
</form>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    var items=[];
       function check(a) {
        let id=$(a).data('id_option');
        let val=$(a).data('val');
        if (val==1) {        
            $("#option").append('<input type="hidden" id="input_'+id+'" value="'+id+'" name="option[]">');

            $("."+id).removeClass('active');
            $(a).addClass('active');
        }else{
            $("#input_"+id).remove();

            $("."+id).removeClass('active');
            $(a).addClass('active');
        }
    }
    function change_other(a){
        if ($(a).prop('checked')==true) {
        $("#input_symptom").attr("disabled",false);

        }else{
            $("#input_symptom").attr("disabled",true);
            $("#input_symptom").val('');

        }
    }
    function change_contact(a) {
      if ($(a).text()=='SI') {
          $(a).addClass('active');
          $("#btn_contact_no").removeClass('active');
          $("#contact").val('Si')
      }else{
        $(a).addClass('active');
        $("#btn_contact_si").removeClass('active');
        $("#contact").val('No')

      }
    }
   
    function send() {
        let total=2;
        var repetidos = 0
        let aux=true;
        if ($("#symptom_other").prop('checked')==true) {
            if ($("#input_symptom").val()== '') {
                aux=false;

            }
        }
        items.forEach(function(numero){
        if(numero==1){
            repetidos++;
        }
        });
        if (repetidos==total && aux==true) {
        $("#section_check").css('display','none');
        $("#section_verification").css('display','block'); 
        }else{
            swal('Items no respondidos','Responda todos los items para poder continuar','error');
        }
    }
    function save(e) {
       e.preventDefault();
       var formdata = new FormData($("#form_check")[0]);
       $.ajax({
           url         : "{{route('store.triage')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
           swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
               }
             }
       }); 
 }
 function back() {
     
    $("#section_check").css('display','block');
    $("#section_verification").css('display','none');

 }
</script>
@endsection
