@extends('layouts.views')
@section('css')
<link rel="preload" as="script" href="{asset('js/decoder.js')}}">

<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

    .opa-rec { 
    white-space: nowrap;
    text-align: center;
    font-family: Segoe UI !important;
    font-style: normal;
    font-weight: bold;
    }

   .font-gr{
       font-weight: 750;
   }
   
 
   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .otro-pregunta-5{
    display: flex;
   }

   .caja-botones{
       width: 100%;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
 
   .card{
     background-color: transparent;
   }

   #qrcode canvas{
    max-width: 100%;
    min-width: 100;
    min-height: 100;
}


   </style>
@endsection
@section('content')

<div class="row mb-4 mt-4 bg-secondary">
  <div class="col-2 col-sm-2 col-md-2">
    <a class="btn " style="background-color: transparent;" onclick="	$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');" href="{{route('home')}}">
          <i class="fa fa-arrow-left fa-2x" style="color:#ffffff; margin-top:9%"></i>
    </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center">
      <h4 class="text-center font-weight-bold text-white mb-0">Mi QR</h4>
    </div>
    <div class="col-2 col-sm-2 col-md-2">
    </div>
</div>

<div class=" row opa-rec" >
  <div class="col-2 col-md-3 col-sm-3" >
  </div>
  <div class="col-8 col-sm-6 col-md-6" >

    <div  class="pr-1 pl-1 card text-black" id="mb-inferior" style="background-color: #ffffff;
    ">
    <p>

      <div id="qrcode">
      </div> 
    </p>
           
      
      </div>
     
    </div>
    <div class="col-2 col-md-3 col-sm-3" >
      
    </div>
    <hr class="line-sep">

 
</div>
<br>
<div class="col-12 col-sm-12 col-md-12 opa-rect" style="     font-family: Segoe UI;
font-style: normal;
font-weight: bold;">
  <div class=" text-center ">
      <span class="text-light" style="font-size: 140%">Nombre y apellidos:</span>
  </div>
  <div class="text-center">
    <h4 class="" style="color: rgba(0,255,203,1);      font-family: Segoe UI;
    font-style: normal;
    font-weight: bold;">{{$user->name}}</h4>
  </div>
  <div class="text-center">
    <span style="font-size: 140%">N° documento de identidad:</span>
    <h4 class="" style="color: rgba(0,255,203,1)">{{$user->ci}}</h4>
</div>
</div>


@endsection
@section('scripts')
<script src="{{asset('js/jquery.qrcode.min.js')}}"></script>
    <script>

      text='{{$user->name}}'+"@@"+ '{{$user->ci}}'+'@@'+'{{$user->id}}';
    	$('#qrcode').qrcode(text);
  
      </script>
      <script async src="https://cdn.jsdelivr.net/npm/pwacompat@2.0.6/pwacompat.min.js" integrity="sha384-GOaSLecPIMCJksN83HLuYf9FToOiQ2Df0+0ntv7ey8zjUHESXhthwvq9hXAZTifA"
        crossorigin="anonymous"></script>
@endsection




