@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-sm-3 col-md-3">
        <a href="{{route('usuario')}}">
            <svg  class="Icon_awesome-arrow-left" viewBox="-9.004 -0 10 10" >
                <path id="Icon_awesome-arrow-left"  d="M 41.50252532958984 68.31076812744141 L 37.92483139038086 71.88845062255859 C 36.40995788574219 73.40333557128906 33.96036529541016 73.40333557128906 32.46160125732422 71.88845062255859 L 1.13264262676239 40.57561111450195 C -0.382235050201416 39.06072998046875 -0.382235050201416 36.61114883422852 1.13264262676239 35.11238479614258 L 32.46160125732422 3.78342342376709 C 33.97648239135742 2.268545866012573 36.42607116699219 2.268545866012573 37.92483139038086 3.78342342376709 L 41.50252532958984 7.361112594604492 C 43.03351593017578 8.892107009887695 43.00128173828125 11.39004421234131 41.43805694580078 12.88880634307861 L 22.01861572265625 31.3896541595459 L 68.33519744873047 31.3896541595459 C 70.47857666015625 31.3896541595459 72.20296478271484 33.11403274536133 72.20296478271484 35.25742340087891 L 72.20296478271484 40.41445541381836 C 72.20296478271484 42.55784606933594 70.47857666015625 44.28223037719727 68.33519744873047 44.28223037719727 L 22.01861572265625 44.28223037719727 L 41.43805694580078 62.7830696105957 C 43.01739501953125 64.28183746337891 43.04962921142578 66.77977752685547 41.50252532958984 68.31076812744141 Z">
                </path>
            </svg>
        </a>
    </div>
    <div class="col-7 col-sm-7 col-md-7">
        <p class="text-center font-weight-bold">Reporte de COVID-19</p>
    </div>
</div>
<!--Fin Encabezado inicial -->
<form action="{{route('store.check')}}" method="POST" onsubmit="save(event)" id="form_check">

<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
        <div>
            <div class="Informacion_Personal">
                <span class="font-gr">Información del trabajador</span>
            </div>
        </div>
        <hr class="line-sep">
        @php
        $count=1;   
       @endphp
        @include('admin.partials.data-user')
    
        @foreach ($options as $option)
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
        <span class="font-gr"> {{$count}}.{{$option->name}}</span>
            <div class="d-flex justify-content-around">
            <button type="button" class="btn btn-success {{$option->id}}" onclick="check(this)" data-val="1" data-id_option={{$option->id}}>SI</button>
            <button type="button" class="btn btn-success {{$option->id}}" onclick="check(this)" data-val="0" data-id_option={{$option->id}} >NO</button>
            </div>
        </div>
        @php
         $count=$count+1;   
        @endphp
        @endforeach
            @csrf
        <div id="option"></div>
        <br>
        <div class="d-flex justify-content-center">
        <button class="btn btn-primary" onclick="send();" type="button">Enviar</button>
        </div>

    </div>
		
</div>
</section>
<section id="section_verification" style="display: none">
    @include('public.partials.confirm')

</section>
</form>

<section id="section_success" style="display: none">
    @include('public.partials.success')
</section>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    var option=[];
    function check(a) {
        let id=$(a).data('id_option');
        let val=$(a).data('val');
        if (val==1) {        
             $("#option").append('<input type="hidden" value="'+id+'" name="option[]">');

            $("."+id).removeClass('active');
            $(a).addClass('active');
        }else{
            $("."+id).removeClass('active');
            $(a).addClass('active');
        }
    }
    function send() {
        $("#section_check").css('display','none');
        $("#section_verification").css('display','block');
    }
    function save(e) {
       e.preventDefault();
       var formdata = new FormData($("#form_check")[0]);
       $.ajax({
           url         : "{{route('store.check')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
           swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
               }
             }
       }); 
 }
 function back() {
     
    $("#section_check").css('display','block');
    $("#section_verification").css('display','none');

 }
</script>
@endsection


