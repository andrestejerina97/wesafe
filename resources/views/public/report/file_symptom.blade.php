@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn-success{
    width: 100px;
    height: 30px;
    filter: drop-shadow(5px 5px 5px rgba(0, 102, 81, 1));
    font-family: Segoe UI;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    border: none;
    background-color: rgba(0,204,162,1) !important;

}
 .active{
    background-color: rgb(7, 131, 106) !important;
}

.inter-lin{
    font-size: 10px;
    margin-left: 15px;
}

@media (max-width: 318px){
    .inter-lin{
    font-size: 80%;
    margin: -10px;
    }
}
@media (min-width: 319px){
    .inter-lin{
    font-size: 75%;
    margin: -10px;

    }
 
}

.btn{
    margin: 6px;
}

#Enviar {
    white-space: nowrap;
    text-align: center;
    font-family: SegoeUI;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    color: rgba(255,255,255,1);
}

#draw-canvas {
  border: 2px dotted #b8b2b2;
  border-radius: 5px;
  cursor: crosshair;
  background-color: rgba(242,242,242,1);
}

#draw-dataUrl {
  width: 100%;
}
h3 {
    margin: 10px 15px;
}


h1 {
    margin: 10px 15px;
}
header {
    color: white;
    font-weight: 500;
    padding-left: 15px;
}


.button {
    background: #3071a9;
    box-shadow: inset 0 -3px 0 rgba(0,0,0,.3);
    font-size: 14px;
    padding: 5px 10px;
    border-radius: 5px;
    margin: 0 15px;
    text-decoration: none;
    color: white;
}

.button:active {
    transform: scale(0.9);
}

.contenedor {
    width: 100%
    margin: 5px;
    display: flex;
    flex-direction: column;
    align-items: center;
}

.instrucciones {
    width: 90%;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items:center;
    margin-bottom: 10px;
}

label {
    margin: 0 15px;
}

footer {
    background: #273B47;
    color: white;
    height: 100%;
    width: 100%;
    margin-top: 10px;
    padding: 0;
    display: flex;
    justify-content: center;
    align-items: center;
}


input[type=range] {
  -webkit-appearance: none;
  margin: 18px 0;

}
input[type=range]:focus {
  outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
  width: 100%;
  height: 8.4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  background: #3071a9;
  border-radius: 1.3px;
  border: 0.2px solid #010101;
}
input[type=range]::-webkit-slider-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 1px solid #000000;
  height: 36px;
  width: 16px;
  border-radius: 3px;
  background: #ffffff;
  cursor: pointer;
  -webkit-appearance: none;
  margin-top: -14px;
}
input[type=range]:focus::-webkit-slider-runnable-track {
  background: #367ebd;
}
input[type=range]::-moz-range-track {
  width: 100%;
  height: 8.4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  background: #3071a9;
  border-radius: 1.3px;
  border: 0.2px solid #010101;
}
input[type=range]::-moz-range-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 1px solid #000000;
  height: 36px;
  width: 16px;
  border-radius: 3px;
  background: #ffffff;
  cursor: pointer;
}
input[type=range]::-ms-track {
  width: 100%;
  height: 8.4px;
  cursor: pointer;
  animate: 0.2s;
  background: transparent;
  border-color: transparent;
  border-width: 16px 0;
  color: transparent;
}
input[type=range]::-ms-fill-lower {
  background: #2a6495;
  border: 0.2px solid #010101;
  border-radius: 2.6px;
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
}
input[type=range]::-ms-fill-upper {
  background: #3071a9;
  border: 0.2px solid #010101;
  border-radius: 2.6px;
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
}
input[type=range]::-ms-thumb {
  box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d;
  border: 1px solid #000000;
  height: 36px;
  width: 16px;
  border-radius: 3px;
  background: #ffffff;
  cursor: pointer;
}
input[type=range]:focus::-ms-fill-lower {
  background: #3071a9;
}
input[type=range]:focus::-ms-fill-upper {
  background: #367ebd;
}


   </style>
@endsection
@section('content')

<form  method="POST" onsubmit="save(event)" action="{{route('store.sworn')}}"  id="form_check">
    @csrf
        <section id="section_check">
<div class="row mb-4 mt-4 bg-secondary">
    <div class="col-2 col-sm-2 col-md-2 ">
        <a class="btn " onclick="$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');" href="{{route('home')}}">
            <i class="fa fa-arrow-left fa-2x" style="color:#ffffff; margin-top:25%"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center align-self-center">
        <h5 class="text-center font-weight-bold text-white mb-0">Ficha sintomatológica de COVID-19</h5>
    </div>
    <div class="col-2 col-sm-2 col-md-2 ">
    </div>
</div>

<div class="opa-rec" >
        <div  class="p-3  card text-black" style="background-color: rgba(204,204,204,0.8)" id="mb-inferior">
            <div>
                <div class="Informacion_Personal">
                    <span class="font-gr">Información del trabajador</span>
                </div>
            </div>
                                <hr class="line-sep"> 
                                @include('admin.partials.data-user')

                <div class="text-center  p-3 mb-2">
                    <span class="font-gr text-left ">
                        <p class="inter-lin">A través de la presente declaración jurada,
                         se compromete a responder con la verdad.</p>
                        <br>
                        <p class="inter-lin">¿En los últimos 14 días calendario ha tenido alguno de los siguientes síntomas?</p>
                        </span>
                </div>
                @php
                $count=1;   
               @endphp
                @foreach ($options as $option)
                    
                
                    <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                        <span class="font-gr"> {{$count}} {{$option->name}} </span>
                    
             

                                @if($option->name=="Está tomando alguna medicación")
                                <div class="d-flex justify-content-around">
                                    <button type="button" class="btn btn-success mb-2 {{$option->id}}" onclick="check(this); document.getElementById('input_medicine').disabled=false;" data-val="1" data-id_option={{$option->id}} >SI</button>
                                    <button type="button" class="btn btn-success {{$option->id}}" onclick="check(this); document.getElementById('input_medicine').disabled=true;document.getElementById('input_medicine').value='';" data-val="0" data-id_option={{$option->id}}>NO</button>
                                    <div class=" input-group-sm mb-2 mt-2">
                                        <input type="text" class="form-control" name="input_medicine" id="input_medicine" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Medicación...">
                                    </div>
                                    
                                </div>
                        

                                @else
                                <div class="d-flex justify-content-around">
                                    <button type="button" class="btn btn-success {{$option->id}}" onclick="check(this)" data-val="1" data-id_option={{$option->id}} >SI</button>
                                    <button type="button" class="btn btn-success {{$option->id}}" onclick="check(this)" data-val="0" data-id_option={{$option->id}}>NO</button>
                             
                                
                                </div>
                                @endif

                    </div>
                    @php
                    $count++;   
                   @endphp
                @endforeach
             
                    
                    
                        <div class="text-center">
                            <span class="font-gr"> Todos los datos expresados en este cuestionario constituyen una declaración jurada de su parte.
                            Asimismo, ha sido informado que, de omitir o falsear información, puede perjudicar la salud de sus compañeros y la mía propia, lo cual, de constituir una falta grave a la salud pública, asume sus consecuencias.</span>
                        
                        </div>

                        <div class="d-flex justify-content-center">
                            <button class="btn " id="Enviar" style="background-color: rgba(67,32,255,1) !important;" type="button" onclick="send();">Enviar</button>
                        </div>
                 
    </div>	

</div>
</section>


<div id="option"></div>


<section id="section_verification" style="display: none">
    @include('public.partials.sign')

</section>
<section id="section_success" style="display: none">
    @include('public.partials.success')
</section>
</form>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    var items=[];
    function check(a) {
        let id=$(a).data('id_option');
        let val=$(a).data('val');
        items[id]=1;
        if (val==1) {        
             $("#option").append('<input type="hidden" id="input_'+id+'" value="'+id+'" name="option[]">');

            $("."+id).removeClass('active');
            $(a).addClass('active');
        }else{

            $("#input_"+id).remove();
            $("."+id).removeClass('active');
            $(a).addClass('active');
        }
    }
    function send() {
        let total={{$count}} - 1;
        var repetidos = 0

        items.forEach(function(numero){
        if(numero==1){
            repetidos++;
        }
        });
        if (repetidos==total) {
        $("#section_check").css('display','none');
        $("#section_verification").css('display','block');   
        }else{
            swal('Items no respondidos','Responda todos los items para poder continuar','error');
        }
    }
    function save(e) {
       e.preventDefault();
       var formdata = new FormData($("#form_check")[0]);
       formdata.append('sign',$("#draw-dataUrl").text());
       $.ajax({
           url         : "{{route('store.sworn')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
           swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
               }
             },
             statusCode:{
               403:function(data) {
           swal("Atención!","Las credenciales proporcionadas no coinciden con los registros actuales","error");
               }
             }
       }); 
 }
 function back() {
     
    $("#section_check").css('display','block');
    $("#section_verification").css('display','none');

 }
 
(function() { // Comenzamos una funcion auto-ejecutable

// Obtenenemos un intervalo regular(Tiempo) en la pamtalla
window.requestAnimFrame = (function (callback) {
    return window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimaitonFrame ||
                function (callback) {
                     window.setTimeout(callback, 1000/60);
        // Retrasa la ejecucion de la funcion para mejorar la experiencia
                };
})();

// Traemos el canvas mediante el id del elemento html
var canvas = document.getElementById("draw-canvas");
var ctx = canvas.getContext("2d");


// Mandamos llamar a los Elemetos interactivos de la Interfaz HTML
var drawText = document.getElementById("draw-dataUrl");
var drawImage = document.getElementById("draw-image");
var clearBtn = document.getElementById("draw-clearBtn");
var submitBtn = document.getElementById("draw-submitBtn");
clearBtn.addEventListener("click", function (e) {
    // Definimos que pasa cuando el boton draw-clearBtn es pulsado
    clearCanvas();
    drawImage.setAttribute("src", "");
}, false);
    // Definimos que pasa cuando el boton draw-submitBtn es pulsado
submitBtn.addEventListener("click", function (e) {
var dataUrl = canvas.toDataURL();
drawText.innerHTML = dataUrl;
$("#form_check").submit();
//drawImage.setAttribute("src", dataUrl);
 }, false);

// Activamos MouseEvent para nuestra pagina
var drawing = false;
var mousePos = { x:0, y:0 };
var lastPos = mousePos;
canvas.addEventListener("mousedown", function (e)
{
/*
  Mas alla de solo llamar a una funcion, usamos function (e){...}
  para mas versatilidad cuando ocurre un evento
*/
    var tint = document.getElementById("color");
    var punta = document.getElementById("puntero");
    console.log(e);
    drawing = true;
    lastPos = getMousePos(canvas, e);
}, false);
canvas.addEventListener("mouseup", function (e)
{
    drawing = false;
}, false);
canvas.addEventListener("mousemove", function (e)
{
    mousePos = getMousePos(canvas, e);
}, false);

// Activamos touchEvent para nuestra pagina
canvas.addEventListener("touchstart", function (e) {
    mousePos = getTouchPos(canvas, e);
console.log(mousePos);
e.preventDefault(); // Prevent scrolling when touching the canvas
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchend", function (e) {
e.preventDefault(); // Prevent scrolling when touching the canvas
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchleave", function (e) {
// Realiza el mismo proceso que touchend en caso de que el dedo se deslice fuera del canvas
e.preventDefault(); // Prevent scrolling when touching the canvas
var mouseEvent = new MouseEvent("mouseup", {});
canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchmove", function (e) {
e.preventDefault(); // Prevent scrolling when touching the canvas
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
}, false);

// Get the position of the mouse relative to the canvas
function getMousePos(canvasDom, mouseEvent) {
    var rect = canvasDom.getBoundingClientRect();
/*
  Devuelve el tamaño de un elemento y su posición relativa respecto
  a la ventana de visualización (viewport).
*/
    return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
    };
}

// Get the position of a touch relative to the canvas
function getTouchPos(canvasDom, touchEvent) {
    var rect = canvasDom.getBoundingClientRect();
console.log(touchEvent);
/*
  Devuelve el tamaño de un elemento y su posición relativa respecto
  a la ventana de visualización (viewport).
*/
    return {
        x: touchEvent.touches[0].clientX - rect.left, // Popiedad de todo evento Touch
        y: touchEvent.touches[0].clientY - rect.top
    };
}

// Draw to the canvas
function renderCanvas() {
    if (drawing) {
  var tint = document.getElementById("color");
  var punta = document.getElementById("puntero");
  ctx.strokeStyle = tint.value;
  ctx.beginPath();
        ctx.moveTo(lastPos.x, lastPos.y);
        ctx.lineTo(mousePos.x, mousePos.y);
  console.log(punta.value);
    ctx.lineWidth = punta.value;
        ctx.stroke();
  ctx.closePath();
        lastPos = mousePos;
    }
}

function clearCanvas() {
    canvas.width = canvas.width;
}

// Allow for animation
(function drawLoop () {
    requestAnimFrame(drawLoop);
    renderCanvas();
})();

})();

</script>
@endsection



