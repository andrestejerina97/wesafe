@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .btn{
       background: rgba(0,204,162,1) !important;
       border-color: rgba(0,204,162,1)!important;
       padding-top: 0px;
       padding-bottom: 0px;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .otro-pregunta-5{
    display: flex;
   }

   .caja-botones{
       width: 100%;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn{
    border-width: 0;
  outline: none;
  border-radius: 10px;
  box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
   }

   </style>
@endsection
@section('content')

<div class="row mb-4 mt-4 bg-secondary">
    <div class="col-2 col-sm-2 col-md-2">
        <a  href="{{route('home')}}" onclick="	$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');">
            <i class="fa fa-arrow-left fa-2x" style="color:#ffffff; margin-top:28%"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8">
        <h4 class="text-center font-weight-bold text-white mb-0">Teléfonos de emergencia</h4>
    </div>
    <div class="col-2 col-sm-2 col-md-2">
    </div>
</div>

<div class="opa-rec" >
    <div  class="p-3  card text-black" style="background-color: rgba(204,204,204,0.8)" id="mb-inferior">
       @foreach ($phones as $phone)
    <a href="tel:{{$phone->number}}">
       <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
       <button type="button" class="btn btn-success d-flex justify-content-around"> <p>{{$phone->description}}</p> <p>{{$phone->number}}</p></button> 
        </div>
      </a>
       @endforeach
    </div>
		

</div>

@endsection


