@extends('layouts.views')
@section('css')
<style>
    


   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px; 
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
  
   </style>
@endsection
@section('content')
<div class="row mb-4 mt-4 bg-secondary">
    <div class="col-2 col-sm-2 col-md-2">
        <a  onclick="	$('#loader').removeClass('disabled');
		$('#menu_home').addClass('loading');location.href='{{route('home') }}'" class="btn btn-secondary" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center">
        <h4 class=" font-weight-bold mb-0 text-white">Protocolos o procedimientos</h4>
    </div>
    <div class="col-2 col-sm-2 col-md-2">

    </div>
  </div>

<div class="opa-rec" >
   <div>
        <div  class="p-3 card text-black" style="background-color: rgba(204,204,204,0.8)" id="mb-inferior">
                <div class="accordion" id="accordionExample">
                    <div class="card">

            @foreach ($protocols as $protocol)
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button onclick="change_icon(this);" data-active='0' class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse_{{$protocol->id}}
                            " aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-folder"></i> {{$protocol->name}}
                        </button>
                    </h2>
                    </div>
                    <div id="collapse_{{$protocol->id}}" class="collapse hidden" aria-labelledby="collapse_{{$protocol->id}}" data-parent="#accordionExample">
                    <div class="card-body d-flex-column justify-content-around">
                        @isset($reports)
                        @foreach ($reports as $report)
                        @if ($report->protocols_id==$protocol->id)
                    <a  href="{{asset('storage/protocols/'.$report->name)}}" download="FlujoGrama-retorno-laboral" style="color:#000000;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> {{$report->name}}</a>
                        @endif
                        @endforeach
                            
                        @endisset
                        <br>
                        <br>
                        
                    </div>
                    </div>
               
                @endforeach

            </div>
                </div>
            </div>
    </div>	

</div>


@endsection
@section('scripts')
<script>
function change_icon(a) {
//$(a).child('<i>').removeClass('fa');
if ($(a).data('active') < 1) {

$( ".btn-block" ).each(function( index ) {
    if ($(this).data('active') > 0) {
    $(this).children('.fa').removeClass('fa-folder-open-o');
    $(this).children('.fa').addClass('fa-folder');
    $(this).data('active',0);

    }
});
//$(a).children('.fa').removeClass('fa-folder')
//$(a).child('<i>').removeClass('fa');
$(a).children('.fa').addClass('fa-folder-open-o')
$(a).data('active',1);
}else{
    $( ".btn-block" ).each(function( index ) {
    if ($(this).data('active') > 0) {
    $(this).children('.fa').removeClass('fa-folder-open-o');
    $(this).children('.fa').addClass('fa-folder');
    $(this).data('active',0);

    }
    });
//$(a).children('.fa').removeClass('fa-folder-open-o')
//$(a).child('<i>').removeClass('fa');
$(a).children('.fa').addClass('fa-folder')
$(a).data('active',0);

}

}    
</script>    
@endsection

