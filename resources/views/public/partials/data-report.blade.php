
<div class="opa-rec" >
    <div  class="p-3 card text-black" style="background-color: rgba(204,204,204,0.8)" id="mb-inferior">
        <div>
            <div class="Informacion_Personal">
                <span class="font-gr">Información del trabajador</span>
            </div>
        </div>
        <hr class="line-sep">
        @include('admin.partials.data-user')
   
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">1. ¿Quisieras indicar el nombre de la persona a reportar?</span>
        </div>
        <div class="row mt-2">
            <div class="col-sm-4 col-md-4 col-4">
                <button type="button" onclick="document.getElementById('input_name').disabled=false; document.getElementById('input_name').focus(); items[1]=1; $('.btn-success').removeClass('active'); $(this).addClass('active');" class="btn btn-success" style="width: 100%">SI</button>
            </div>
            <div class="col-sm-8 col-md-8 col-8">
                <div class="input-group input-group-sm mb-1">
                    <input type="text" name="name" id="input_name" disabled class="form-control"  aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
        </div>
    </div>
        <div class="row mt-2">
            <div class="col-sm-4 col-md-4 col-4">
                <button type="button" class="btn btn-success" onclick="document.getElementById('input_name').disabled=true; document.getElementById('input_name').value=''; items[1]=1;  $('.btn-success').removeClass('active'); $(this).addClass('active');" style="width: 100%">NO</button>
            </div>

        </div>

    <div class="mt-3">
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">2. ¿Cómo te enteraste de que dicha persona tiene COVID-19?</span>
        </div> 
        <div>
            <textarea name="detail" id="detail" placeholder="Escribe el detalle..." class="form-control mt-2" rows="3"></textarea>
            
    </div>
</div>
    <div class="mt-3">
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">3. ¿Qué vínculo tienes con la persona reportada?</span>
        </div> 
    </div>
    <div class="form-check mt-2">     
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_other();items[3]=1;" name="link_family" id="blankCheckbox" value="Compañero de trabajo" aria-label="..."><span> Compañero de trabajo</span>
            </div>
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_other();items[3]=1;" name="link_family" id="blankCheckbox" value="Familiar que vive en mi casa" aria-label="..."><span>Familiar que vive en mi casa</span>
            </div>
            
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_other();items[3]=1;" name="link_family" id="blankCheckbox" value="Familiar que visité o estuvo de visita" aria-label="..."><span>Familiar que visité o estuvo de visita</span>
            </div>
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_other();items[3]=1;" name="link_family" id="blankCheckbox" value="Amigo(a) que visité o estuvo de visita" aria-label="..."><span>Amigo(a) que visité o estuvo de visita</span>
            </div>
            <div class="otro-pregunta-5">
                <input class="form-check-input position-static" onclick="document.getElementById('input_link_family').disabled=false;items[3]=1;" type="radio" name="link_family" value="" aria-label="..."><span>Otro</span>
                <div class="input-group input-group-sm mb-1 ml-4">
                    <input type="text" placeholder="ingrese vínculo..." class="form-control" id="input_link_family" name="link_family" disabled aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
    </div>
        
    <div class="mt-3">
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">4. ¿Cuándo fue la última vez que tuvo contacto físico con dicha persona?</span>
        </div> 
    </div>
 <div class="row mt-2">
    <div class="col-6 col-sm-6 col-md-6">
        <div class="input-group input-group-sm">
            <input type="date" class="form-control" name="last_contact" id="last_contact" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
        </div>
    </div>
 </div>
    
    <div class="mt-3">
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">5. ¿Cuánto tiempo tuvo contacto con dicha persona?</span>
            </div> 
    </div>
    <div class="form-check mt-2">     
            <div>
                <input class="form-check-input position-static" onclick="change_time();items[5]=1;" type="radio" name="time_contact" id="blankCheckbox" value="Menos de 2 horas" aria-label="..."><span> Menos de 2 horas</span>
            </div>
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_time();items[5]=1;"  name="time_contact" id="blankCheckbox" value="De 2 a 8 horas" aria-label="..."><span>De 2 a 8 horas</span>
            </div>
            
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_time();items[5]=1;" name="time_contact" id="blankCheckbox" value="Más de 8 horas" aria-label="..."><span>Más de 8 horas</span>
            </div>
            <div>
                <input class="form-check-input position-static" type="radio" onclick="change_time();items[5]=1;" name="time_contact" id="blankCheckbox" value="Amigo(a) que visité o estuvo de visita" name="last_contact" aria-label="..."><span>Amigo(a) que visité o estuvo de visita</span>
            </div>
            <div class="otro-pregunta-5">
                <input class="form-check-input position-static" type="radio" onclick="document.getElementById('input_time_contact').disabled=false;items[5]=1;" name="time_contact"  id="blankCheckbox" value="otro" aria-label="..."><span>Otro</span>
                <div class="input-group input-group-sm mb-1">
                    <input type="text" class="form-control"  aria-label="Sizing example input" id="input_time_contact" name="input_time_contact" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
    </div>
    <br>
        <div class="d-flex justify-content-center">
        <button id="Enviar" class="btn mt-3 text-center" style="background-color: rgba(67,32,255,1) !important;" onclick="send()" type="button">Enviar</button>
        </div>
		
</div>
</div>
