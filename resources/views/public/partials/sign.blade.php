<div class="row mb-4 mt-4 align-items-start" style="background-color: transparent !important">
    <div class="col-2 col-sm-2 col-md-2">
        <a class="btn " style="background-color: transparent;" onclick="onclick=back();">
            <i class="fa fa-arrow-left fa-2x" style="color:#ffffff; margin-top:6%"></i>
        </a>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center">
    </div>
    <div class="col-2 col-sm-2 col-md-2">
    </div>
</div>
<div class="form-group mt-4 text-center">
    <a href="#" class="text-center "><img src="{{asset('img/logo.png')}}"  style="width: 40%"></a>
 </div>

<div class="contenedor">
    <h5 class="text-light text-center">
        Mediante mi firma declaro bajo juramento que la información previamente brindada es veráz
    </h5>
    <div class="row">
        <div class="col-md-12">
             <canvas id="draw-canvas" width="300" height="300">
                 No tienes un buen navegador.
             </canvas>
            
         </div>
    </div>
    <div class="row">
         <small class="text-light text-center">
                Ingrese su firma dentro del recuadro. Esta debe ser idéntica a la registrada en su documento de identidad
             </small>
        <div class="col-md-12 text-center">
            <input type="button" class="button" style="background-color: rgba(0,204,162,1) !important; width:70%;border:none;" id="draw-clearBtn" value="Borrar"></input>
                    <input type="color" hidden value="#000000" id="color">
                    <input type="range" hidden id="puntero" min="1" default="1" max="5" width="10%">
        </div>
        <div class="col-md-12 text-center mt-2">
            <input type="button" class="button" style="background-color: rgba(67,32,255,1) !important; width:70% ; border:none;"  id="draw-submitBtn" value="Enviar"></input>

        </div>

    </div>

    <br/>

    <br/>
    <div class="">
        <div class="col-md-12">
            <img id="draw-image" width="60%" height="60%" src="" alt=""/>
        </div>
    </div>
        <div class="row">
        <div class="col-md-12">
            <textarea id="draw-dataUrl" class="form-control" rows="1" hidden></textarea>
        </div>
    </div>
</div>