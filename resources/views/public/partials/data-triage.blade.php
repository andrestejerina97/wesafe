
    <div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
                <span class="font-gr">1. ¿Cómo te sientes hoy? Indica si tienes alguno de los siguientes síntomas:</span>
            </div> 
    </div>
        <div class="form-check">     
            @foreach ($options as $option)
         
            @if($option->name=='Otro')
            <div class="otro-pregunta-5">
                <input class="form-check-input position-static" id="symptom_other" onclick="change_other(this);items[1]=1;" type="checkbox" name="symptom[]" value="{{$option->id}}" aria-label="..."><span>Otro</span>
                <div class="input-group input-group-sm mb-1 ml-4">
                    <input type="text" placeholder="Indique otro síntoma.." class="form-control" id="input_symptom"  name="input_symptom" disabled aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                </div>
            </div>
            @else
            <div>
                <input class="form-check-input position-static" onclick="items[1]=1;"  name="symptom[]" type="checkbox" id="symptom" value="{{$option->id}}" aria-label="..."><span>{{$option->name}}</span>
                </div>
            @endif 
            @endforeach
           
        </div>
        <div class="ID1_Cmo_te_sientes_hoy_Indica_">
        <span class="font-gr"> 2. Desde su última evaluación ¿Ha tenido contacto con una o más personas con caso confirmado de COVID-19?</span>
        
        <div class="d-flex justify-content-around">
        <button  class="btn btn-success" type="button" id="btn_contact_si" onclick="change_contact(this);items[2]=1;" class="btn btn-success" >SI</button>
        <button class="btn btn-success" type="button" id="btn_contact_no" onclick="change_contact(this);items[2]=1;" class="btn btn-success">NO</button>
        </div>
        
        <div class="d-flex justify-content-center">
        <button id="Enviar" class="btn  mt-3 text-center" style="background-color: rgba(67,32,255,1) !important;" onclick="send();" type="button">Enviar</button>
        </div>
    </div>
		