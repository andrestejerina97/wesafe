<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Wesafe- QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Una aplicación para combatir el COVID 19">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
	<title>WeSafe</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<style>
        #main{

            
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            color: rgba(255,255,255,1);
               }
        #rectangule{
		background-color: rgb(19, 17, 17);
        /* Full height */
        height: 65%;
        /* Center and scale the image nicely */
        display: block;
        background-position: center;
        background-repeat: no-repeat;
        opacity: 0.8;
        overflow: visible;

        }
    </style>
	@yield('css')

</head><!--/head-->
<body style=" background-image: url('{{asset('img/fondo_covid.png')}}');background-repeat: no-repeat;" >
    <div id="rectangule">
    </div>
    <div id="main" style="display: none" class="container">
<br>
        <div class="row mt-4">
			<div class="col-lg-3 col-md-3 col-xs-3 col-3"></div>
			<div class="col-lg-6 col-md-6 col-xs-6 col-6">
                <div class="text-center">
                 <img src="{{asset('img/logo.png')}}" style="max-height: 60%; max-width:60%" alt="">
                 

                </div>
			</div>
			<div class="col-lg-3 col-md-3 col-xs-3 col-3"></div>

        </div>
        <div class="row mt-3">
			<div class="col-lg-1 col-md-1 col-xs-1 col-1"></div>
			<div class="col-lg-10 col-md-10 col-xs-10 col-10">
                <div class="text-center">
                    <h5 class="text-home">¡Te damos la bienvenida!</h5>
                    <p>Por favor inicia sesión para continuar</p>                 
                </div>
			</div>
			<div class="col-lg-1 col-md-1 col-xs-1 col-1"></div>

        </div>
       
        @yield('content')
    </div>
		
	


<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>

      <!-- end - carousel logos empresas -->  
    
    @yield('scripts')
    
</body>
</html>