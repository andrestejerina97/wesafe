<!DOCTYPE html>
<html lang="es">
<head> 
    <meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Wesafe- QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Una aplicación para combatir el COVID 19">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
	<title>WeSafe</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<style>
        #main{
            background-image: url("{{asset('img/fondo_covid.png')}}");
            background-repeat: no-repeat;
            display:block;
            position:fixed;
            width:100%;
            height:100%;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            color: rgba(255,255,255,1);
               }
        #rectangule{
		background-color: rgb(19, 17, 17);
        /* Full height */
        height: 65%;
        /* Center and scale the image nicely */
        display: block;
        background-position: center;
        background-repeat: no-repeat;
        opacity: 0.8;
        overflow: visible;

        }
    </style>
	@yield('css')

</head><!--/head-->
<body >
    <div id="rectangule">
    </div>
    <div id="main" style="display: none" class="container">
<br>
 

       
        @yield('content')
    </div>
		
	


<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script>
    $(function() {
        $("#main").css('display','block');
    })
</script>
      <!-- end - carousel logos empresas -->  
    
    @yield('scripts')
    
</body>
</html>