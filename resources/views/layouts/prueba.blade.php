<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
	<meta name="author" content="Wesafe- QuodSystem"> 
	<title>WeSafe</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="{{asset('css/adminlte.min.css')}}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->
        <style>
            #main{
                background-repeat: no-repeat;
                display:block;
                position:fixed;
                width:100%;
                height:100%;
                font-family: Segoe UI;
                font-style: normal;
                font-weight: normal;
                color: rgba(255,255,255,1);
                   }
            .main-header{
            background-color: rgb(19, 17, 17);
            /* Full height */
            height: 65%;
            width: 90%;
            margin-left: 5% !important;
            margin-top: 5% !important;
            justify-content: center;
            /* Center and scale the image nicely */
            display: block;
            background-position: center;
           
            opacity: 0.8;
            overflow: visible;
            border: none;
    
            }
    .btn-login{
        white-space: nowrap;
            text-align: center;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 115%;
            background-color: #4300ff !important;
            border-width: 0;
            border-radius: 2px;
            box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
            transition: background-color .3s;
            overflow: hidden;
    
    }
    .btn-login:hover, .btn-login:focus {
      background-color: #200f50;
    }
    .rectangule-initial{
        position: fixed;
        top: 15%;
        bottom: 50%;
    }
  
    
        </style>

	@yield('css')

</head><!--/head-->
<body class="hold-transition sidebar-mini" style="background-image: url('img/fondo_covid.png'); min-height:250%;">
    <div class="wrapper" >
  <!-- Main Sidebar Container -->
  <!-- Navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: transparent;">
    <!-- Content Header (Page header) -->
   
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" >
      <div class="container-fluid"  >
        <div class="row" >
            @yield('content')

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


    </div>
		
	</div>

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: "6f3f7aaf-7630-4573-9b79-a8d2209c8393",
      });
    });
  </script> 
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/popper.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/adminlte.min.js')}}"></script>
      <!-- end - carousel logos empresas -->  
  
    @yield('scripts')
    
</body>
</html>