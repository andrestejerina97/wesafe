<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Wesafe- QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Una aplicación para combatir el COVID 19">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
	<title>WeSafe</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="{{asset('css/adminlte.min.css')}}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->
        <style>
            #main{
                background-repeat: no-repeat;
                display:block;
                position:fixed;
                width:100%;
                height:100%;
                font-family: Segoe UI;
                font-style: normal;
                font-weight: normal;
                color: rgba(255,255,255,1);
                   }
            .main-header{
            background-color: rgb(19, 17, 17);
            /* Full height */
            height: 65%;
            width: 90%;
            margin-left: 5% !important;
            margin-top: 5% !important;
            justify-content: center;
            /* Center and scale the image nicely */
            display: block;
            background-position: center;
           
            opacity: 0.8;
            overflow: visible;
            border: none;
    
            }
    .btn-login{
        white-space: nowrap;
            text-align: center;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 115%;
            background-color: #4300ff !important;
            border-width: 0;
            border-radius: 2px;
            box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
            transition: background-color .3s;
            overflow: hidden;
    
    }
    .btn-login:hover, .btn-login:focus {
      background-color: #200f50;
    }
    .rectangule-initial{
        position: fixed;
        top: 15%;
        bottom: 50%;
    }
  
  .lds-ellipsis {
  display: inline-block;
  position: fixed;
  width: 80px;
  height: 80px;
  left: 43%;
  top: 50%;
}
.lds-ellipsis div {
  position: absolute;
  top: 33px;
  width: 13px;
  height: 13px;
  border-radius: 50%;
  background: #fff;
  animation-timing-function: cubic-bezier(0, 1, 1, 0);
}
.lds-ellipsis div:nth-child(1) {
  left: 8px;
  animation: lds-ellipsis1 0.6s infinite;
}
.lds-ellipsis div:nth-child(2) {
  left: 8px;
  animation: lds-ellipsis2 0.6s infinite;
}
.lds-ellipsis div:nth-child(3) {
  left: 32px;
  animation: lds-ellipsis2 0.6s infinite;
}
.lds-ellipsis div:nth-child(4) {
  left: 56px;
  animation: lds-ellipsis3 0.6s infinite;
}
@keyframes lds-ellipsis1 {
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
}
@keyframes lds-ellipsis3 {
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  }
}
@keyframes lds-ellipsis2 {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(24px, 0);
  }
}

.disabled{
  display: none;
}
.loading{
  opacity: 0.5;
}
        </style>

	@yield('css')

</head><!--/head-->
<body class="hold-transition sidebar-mini " id="menu" style="background-image: url({{asset('img/fondo_covid.png')}}); min-height:250%;">
   
  <div id="loader" class="lds-ellipsis disabled"><div></div><div></div><div></div><div></div></div>  
  <div class="wrapper" >
  <!-- Main Sidebar Container -->
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item mx-auto mt-2">
          <span class="text-light text-center" style="font-weight: 800; color: #ffffff;">Menú de opciones</span>
    </li>
      <li class="nav-item dropdown ">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell"></i>
          <span class="badge badge-warning navbar-badge"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-header">15 Notificaciones</span>
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>

 
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-image: url({{asset('img/fondo_covid.png')}})";">
 

    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user panel (optional) -->
     

      <!-- Sidebar Menu -->
      <nav class="mt-2" >
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <div class="row mb-4 mt-4 bg-secondary text-white ">
               
                <div class="col-8 col-xs-8 col-md-8">
                    <p class="text-center font-weight-bold" style="font-size: 100%">Configuración</p>
                </div>
                <div class="col-3 col-xs-3 col-md-3">
                  <a class=" btn text-left" data-widget="pushmenu" style="background-color: transparent; width:100%; height:100%; border:none !important;" type="button" >
                <i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
                  </a>
              </div>
              </div>
               <ul class="list-unstyled components mb-5">
                <li class="nav-item">
                  <a  class="nav-link text-center">
                <img style="border-radius: 50%; width: 50%;" src="{{asset('img/avatar.png')}}" alt="">
              </a>
              </li>
                   <li class="nav-item text-center">
                   <a >Nombre y apellido: {{Auth::user()->name}}</a>
                   </li>
                   <li class="nav-item text-center">
                    <a  >Documento: {{Auth::user()->ci}}</a>
                    </li>
                    <li class="nav-item">
                      <a  class="nav-link" href="{{ route('password.request') }}"><i class="fa fa-key" aria-hidden="true"></i>
                        Cambiar contraseña</a>
                      </li>
                      <li class="nav-item">
                        <a  class="nav-link"> 
                          <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="customSwitch1">
                          <label class="custom-control-label" for="customSwitch1">Notificaciones</label>
                        </div> 
                      </a>
                        </li>
                   <li>
                   <form id="form_logout" action="{{route('logout')}}" method="POST">
                  @csrf
                  </form>
                       <a href="javascript:;" onclick="document.getElementById('form_logout').submit();" class="nav-link">Cerrar sesión</a>
                   </li>      
        </ul>
 
      </nav>
 
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
    <div class="text-center" style="position: relative !important; bottom: -150px;">
      <a href="#" class="text-center"><img src="{{asset('img/logo.png')}}"  style="width: 50%"></a>
    </div>  
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-color: transparent;">
    <!-- Content Header (Page header) -->
   
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content" >
      <div class="container-fluid"  >
        <div class="row" >
            @yield('content')

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


    </div>
		
	</div>

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/popper.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/adminlte.min.js')}}"></script>
<script src="{{asset('js/pwabuilder-sw.js')}}"></script>
<script  type="module"  src="{{asset('js/pwabuilder-sw-register.js')}}"></script>
      <!-- end - carousel logos empresas -->  
      <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
          OneSignal.init({
            appId: "6f3f7aaf-7630-4573-9b79-a8d2209c8393",
          });
        });
        $(window).on("beforeunload", function() { 
            $("#loader").removeClass('disabled');
          });
      </script> 
    @yield('scripts')
    
</body>
</html>