<!DOCTYPE html>
<html lang="es">
<head> 
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Wesafe- QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Una aplicación para combatir el COVID 19">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">

	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->


	<link href="{{asset('css/admin.css')}}" rel="stylesheet">	
	<link href={{asset('css/responsive.css')}} rel="stylesheet"> 
	<link href="{{asset('css/mapa.css')}}" rel="stylesheet"> 
    

<link href="{{asset('css/infinite-slider.css')}}" rel="stylesheet">
<link href="{{asset('css/social.css')}}" rel="stylesheet"> 
    
<link href="{{asset('css/animate.css')}}" rel="stylesheet"> 
<link href="{{asset('css/style.css')}}" rel="stylesheet"> 
@yield('css')
<script src="{{asset('js/wow.min.js')}}"></script>

	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script> 
	<script src="js/respond.min.js"></script> <![endif]--> 
	<link rel="shortcut icon" href="favicon.ico"> 
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png"> 
	<link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
		<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-SWGHXDL85E"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-SWGHXDL85E');
	</script>
	
	<script>
	 new WOW().init();
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	@include('sweetalert::alert')
	
	
</head><!--/head-->
<body>
    <header id="navigation"> 
		<div class="navbar navbar-inverse navbar-fixed-top" role="banner"> 
			<div class="container"> 
				<div class="navbar-header"> 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> 
						<span class="sr-only">Toggle navigation</span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
					</button> 
					<a href="{{route('admin')}}"><h3 class="page-scroll" >ADMINISTRACIÓN</h3>  </a>
					                    
				</div> 
				
			</div>			
		</div><!--/navbar--> 
	</header> <!--/#navigation--> 
	<div class="preloader">
		<div class="preloder-wrap">
			<div class="preloder-inner"> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
				<div class="ball"></div> 
		
			</div>
		</div>
	</div><!--/.preloader-->
    <section id="home">
        <br>
        <br>
   

	</section><!--/#home-->
  
    <section id="admin" class="about-area area-padding">
  
         
        <div class="row">
            <div class="">
				
                <div class="sidebar">
					<a href="#" class="img-side"><h1><span ><img src="{{asset('images/iconos/puenteblanco.jpeg')}}"  style="width: 90%"></span></h1></a>

                    <div class="mt-1 ">
                    <a  href="{{route('users.index')}}"><i class="fa fa-users fa-2x"></i> <span class="nav-text">Usuarios</span></a>
                    <a href="{{route('rankins.index')}}"><i class="fa fa-bookmark fa-2x"></i> <span class="nav-text">Rankins</span></a>
					<a href="{{route('cases.index')}}"><i class="fa fa-user fa-2x "></i> <span class="nav-text">Casos</span></a>
					<a href="{{route('proyects.index')}}"><i class="fa fa-globe fa-2x" aria-hidden="true"></i> <span class="nav-text">Proyectos</span></a>
                    <a href="{{route('oportunities.index')}}"><i class="fa fa-bullhorn fa-2x"></i> <span class="nav-text">Oportunidades</span></a>

                    </div>
                  </div>
                @yield('content')
            </div>          
        
		  </div>
		  
		  
		  
        </section>
 

	

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/smoothscroll.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.isotope.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/jquery.prettyPhoto.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/jquery.parallax.js')}}"></script> 
	<script type="text/javascript" src="{{asset('js/main.js')}}"></script> 
	<script src="{{asset('js/wow.min.js')}}"></script>
	<script src="{{asset('js/pwabuilder-sw.js')}}"></script>
	<script  type="module"  src="{{asset('js/pwabuilder-sw-register.js')}}"></script>
    @yield('scripts')
    
</body>
</html>