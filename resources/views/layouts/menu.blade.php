<!DOCTYPE html>
<html lang="es">
<head> 
  <meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<meta name="description" content="Wesafe la mejor opción para opción para tu empresa " /> 
    <meta name="author" content="Wesafe- QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#000000"/>
    <meta name="description" content="Una aplicación para combatir el COVID 19">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("icon-152x152.png")}}">
	<title>WeSafe</title> 
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->

		<!-- Global site tag (gtag.js) - Google Analytics -->
        <style>
            #main{
                background-image: url('img/fondo_covid.png');
                background-repeat: no-repeat;
                display:block;
                position:fixed;
                width:100%;
                height:100%;
                font-family: Segoe UI;
                font-style: normal;
                font-weight: normal;
                color: rgba(255,255,255,1);
                   }
            #rectangule{
            background-color: rgb(19, 17, 17);
            /* Full height */
            height: 65%;
            /* Center and scale the image nicely */
            display: block;
            background-position: center;
            background-repeat: no-repeat;
            opacity: 0.8;
            overflow: visible;
    
            }
    .btn-login{
        white-space: nowrap;
            text-align: center;
            font-family: Segoe UI;
            font-style: normal;
            font-weight: normal;
            font-size: 125%;
            background-color: #4300ff !important;
            border-width: 0;
            border-radius: 2px;
            box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
            transition: background-color .3s;
            overflow: hidden;
    
    }
    .btn-login:hover, .btn-login:focus {
      background-color: #200f50;
    }
    .rectangule-initial{
        position: fixed;
        top: 15%;
        bottom: 50%;
    }
    #content {
  width: 100%;
  padding: 0;
  min-height: 100vh;
  
  -webkit-transition: all 0.3s;
  -o-transition: all 0.3s;
  transition: all 0.3s;
  background-image: url('img/fondo_covid.png');
  background-repeat: no-repeat;
  font-family: Segoe UI;
    font-style: normal;
    font-weight: normal;
    color: rgba(255,255,255,1);
}
    
        </style>

	@yield('css')

</head><!--/head-->
<body >
    <div class="wrapper d-flex align-items-stretch main">
        <nav id="sidebar" style="  background-image: url('img/fondo_covid.png');
        background-repeat: no-repeat;">
            <div class="p-4 pt-5">
              <a href="#" class="img logo rounded-circle mb-5" style="background-image: url(img/avatar.png);"></a>
        <ul class="list-unstyled components mb-5">

            <li>
                <a href="#">Configuración hola</a>
            </li>
            <li>
                <a href="#">Cerrar sesión</a>
            </li>
    

        <div class="footer">
        
        </div>

      </div>
    </nav>
	
    <div id="content" class="p-4 p-md-3">
        
        <nav class="navbar navbar-expand-lg navbar-light bg-dark ">
            <div class="container-fluid">
  
              <button type="button" id="sidebarCollapse" class="btn btn-dark">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
              </button>
             
  
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto">
                  <li class="nav-item active">
                      <a class="nav-link" href="#">Home</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">About</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">Portfolio</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">Contact</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
		<!--Navbar responsive mobile -->
		
	<!-- End navbar responsive mobile-->
        @yield('content')
    </div>
		
	</div>

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: "6f3f7aaf-7630-4573-9b79-a8d2209c8393",
      });
    });
  </script> 
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/popper.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
      <!-- end - carousel logos empresas -->  
  
    @yield('scripts')
    
</body>
</html>