@extends('layouts.app')
@section('css')
<style>
.btn-login{
    white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: normal;
		font-size: 125%;
		background-color: #4300ff !important;
        border-width: 0;
        border-radius: 2px;
        box-shadow: 0 1px 4px rgba(0, 0, 0, .6);
        transition: background-color .3s;
        overflow: hidden;

}
.btn-login:hover, .btn-login:focus {
  background-color: #200f50;
}
</style>    
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-10 col-md-10 col-lg-10 ">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Usuario</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @if (Route::has('password.request'))
                                <a class="btn " style="color: #00ffcb; font-size: 95%" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <input class="form-check-input" hidden type="checkbox" name="remember" id="remember" checked>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 text-center">
                                <button type="submit" class="btn btn-primary btn-login">
                                    Iniciar sesión
                                </button>

                               
                            </div>
                        </div>
                    </form>
        </div>
    </div>
</div>
@endsection
