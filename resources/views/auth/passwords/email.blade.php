@extends('layouts.verify')
@section('css')
   <style>
    .card{
        border-radius: 10px;
        opacity: 0.8;
       color: black;
       margin: 15px;
       
    }
    .entry{
        background-color: #ffffff !important; 
    }
   </style>
@endsection

@section('content')
<div class="container">
    <div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <button class=" btn text-left" onclick="location.href='{{ url()->previous() }}'" style="background-color: transparent; width:100%; height:100%; border:none !important;" type="button" >
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-8 col-xs-8 col-md-8">
        <p class="text-center font-weight-bold" style="font-size: 130%">Restablecer contraseña</p>
    </div>
  </div>

  <div class="row mt-4">
    <div class="col-lg-3 col-md-3 col-xs-3 col-3"></div>
    <div class="col-lg-6 col-md-6 col-xs-6 col-6">
        <div class="text-center">
         <img src="{{asset('img/logo.png')}}" style="max-height: 60%; max-width:60%" alt="">
         

        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-xs-3 col-3"></div>

</div>
    <div class="row justify-content-center">
        <div class="col-md-8">
<div class="card">
    <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4  text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control entry @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-6 offset-md-4 mt-4">
                                <button type="submit" class="btn" style="background-color: rgba(67,32,255,1); color: #ffffff !important;">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
