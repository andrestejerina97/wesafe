@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn{
       background: rgba(0,204,162,1) !important;
       border-color: rgba(0,204,162,1)!important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .otro-pregunta-5{
    display: flex;
   }

   .caja-botones{
       width: 100%;
   }
   </style>
@endsection
@section('content')


<div class="p-3 mb-4 mt-4 bg-secondary text-white">
    <p class="text-center font-weight-bold">Teléfonos de emergencia</p>
</div>






<div class="opa-rec" >
    <div>
        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Policía nacional</p> <p>105</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Bomberos y paramédicos</p> <p>116</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Cruz Roja</p> <p>115</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Defensa Civil</p> <p>110</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>SAMU</p> <p>106</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Línea integral de seguridad
y emergencias</p> <p>911</p></button> 
        </div>

        <div class="d-flex justify-content-center d-flex flex-column bd-highlight mb-3">
        <button type="button" class="btn btn-success d-flex justify-content-around"><p>Success</p> <p>Línea exclusiva para personas
con síntomas de covid-19</p> <p>113</p></button> 
        </div>

    </div>
		

</div>

@endsection


