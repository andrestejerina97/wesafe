@extends('layouts.views')
@section('css')
<style>
    


   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px; 
   }
   
   </style>
@endsection
@section('content')


<div class="p-3 mb-4 mt-4 bg-secondary text-white">
    <p class="text-center font-weight-bold">Ficha sintomatológica de COVID-19</p>
</div>

<div class="opa-rec" >
   <div>
        <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
                <div class="accordion" id="accordionExample">
                    @foreach ($protocols as $protocol)
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse_{{$protocol->id}}
                            " aria-expanded="true" aria-controls="collapseOne">
                           {{$protocol->name}}
                        </button>
                    </h2>
                    </div>
                    <div id="collapse_{{$protocol->id}}" class="collapse show" aria-labelledby="collapse_{{$protocol->id}}" data-parent="#accordionExample">
                    <div class="card-body d-flex-column justify-content-around">
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de ambientes.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Inspección de materiales de higiene.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Lavado de manos.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Desinfección de SSHH.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de vehículos.</button>
                    </div>
                    </div>
                </div>
                @endforeach

                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Protocolos Médicos
                        </button>
                    </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body d-flex-column justify-content-around">
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de ambientes.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Inspección de materiales de higiene.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Lavado de manos.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Desinfección de SSHH.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de vehículos.</button>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Protocolos de Emergencia
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body d-flex-column justify-content-around">
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de ambientes.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Inspección de materiales de higiene.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Lavado de manos.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Desinfección de SSHH.</button>
                        <br>
                        <br>
                        <button type="button" class="btn btn-danger btn-sm">Limpieza y desinfección de vehículos.</button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </div>	

</div>


@endsection


