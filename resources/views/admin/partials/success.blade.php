<div class="container">
    <div class="row justify-content-center">
        <div class="text-center">
            <img src="{{asset('img/logo.png')}}" style="max-height: 50%; max-width:60%" alt="">
           </div>
        <div class="col-10 col-md-10 col-lg-10 ">
            <div class="row text-center">
                <p style="color: azure">La información se ha enviado
                    correctamente</p>
            </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 text-center">
                            <label for="">Fecha de envío: <span id="date_success"></span></label>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 text-center">
                            <label for="">Hora de envío: <span id="time_success"></label>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 text-center">
                            <button type="button" onclick="	$('#loader').removeClass('disabled');
                            $('#menu_home').addClass('loading'); location.href='{{route('home')}}'" class="btn btn-login" style="background-color: #00cca2 !important; color:#ffffff;">
                                   Regresar al menú
                                </button>
  
                            </div>
                        </div>
                  
        </div>
    </div>
  </div>