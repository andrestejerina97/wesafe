@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .card{
       border-radius: 10% !important;
       background-color: rgba(204,204,204,0.8) !important;
   }
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-sm-3 col-md-3">
        <a href="{{route('supervisor.triage')}}">
            <svg  class="Icon_awesome-arrow-left" viewBox="-9.004 -0 10 10" >
                <path id="Icon_awesome-arrow-left"  d="M 41.50252532958984 68.31076812744141 L 37.92483139038086 71.88845062255859 C 36.40995788574219 73.40333557128906 33.96036529541016 73.40333557128906 32.46160125732422 71.88845062255859 L 1.13264262676239 40.57561111450195 C -0.382235050201416 39.06072998046875 -0.382235050201416 36.61114883422852 1.13264262676239 35.11238479614258 L 32.46160125732422 3.78342342376709 C 33.97648239135742 2.268545866012573 36.42607116699219 2.268545866012573 37.92483139038086 3.78342342376709 L 41.50252532958984 7.361112594604492 C 43.03351593017578 8.892107009887695 43.00128173828125 11.39004421234131 41.43805694580078 12.88880634307861 L 22.01861572265625 31.3896541595459 L 68.33519744873047 31.3896541595459 C 70.47857666015625 31.3896541595459 72.20296478271484 33.11403274536133 72.20296478271484 35.25742340087891 L 72.20296478271484 40.41445541381836 C 72.20296478271484 42.55784606933594 70.47857666015625 44.28223037719727 68.33519744873047 44.28223037719727 L 22.01861572265625 44.28223037719727 L 41.43805694580078 62.7830696105957 C 43.01739501953125 64.28183746337891 43.04962921142578 66.77977752685547 41.50252532958984 68.31076812744141 Z">
                </path>
            </svg>
        </a>
    </div>
    <div class="col-7 col-sm-7 col-md-7">
        <p class="text-center font-weight-bold">Fichas sintomatológicas registradas</p>
    </div>
</div>
<!--Fin Encabezado inicial -->
<form action="{{route('supervisor.file')}}" method="POST" onsubmit="save(event)" id="form_check">
<div class="ml-3 d-flex">
    <div class="input-group">
    <label for="">Indique la fecha</label>
    <input type="date" name="date" class="form-control col-6" value="{{$date}}" onchange="select_date(this)">
    </div>
</div>
<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
        <hr class="line-sep">
        @foreach ($reviews as $review)
    <a href="{{route('supervisor.file.filter',$review->id)}}" style="color: #000000; font-size: 120%;">
        <div>
            <div>
                <span>Registrado por:</span>
            <span>{{$review->name}}</span>
            </div>
        <div>
            <span>Fecha:</span>
            <span>{{date('Y-m-d', strtotime($review->created_at))}}</span>
        </div>
        <div>
            <span>Hora:</span>
            <span>{{date('H:i:s', strtotime($review->created_at))}}</span>
        </div>
        <hr class="line-sep">
        </div>
    </a>
        @endforeach
            @csrf
        <div id="option"></div>
        <br>
        <div class="mx-auto">
            <ul class="pagination ">
                <li ><a class="last_page btn btn-primary" href="{{$reviews->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
                <li ><a class="last_page btn " href="javascript:;">{{$reviews->currentPage()}}</a></li>
                <li><a class="next_page btn btn-primary" href="{{$reviews->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
              </ul>
        </div>
 
    </div>
		
</div>
</section>
<section id="section_verification" style="display: none">
    @include('public.partials.confirm')

</section>
</form>

<section id="section_success" style="display: none">
    @include('public.partials.success')
</section>
@endsection
@section('scripts')
<script>
    function select_date(a) {
       url='{{route('supervisor.file.review',["date"=> 0])}}';
       url=url.replace('0',$(a).val());
       location.href=url;
        
    }
</script>
@endsection


