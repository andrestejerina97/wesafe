@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 100%;
		height: 5%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn-success{
    width: 100px;
    height: 30px;
    filter: drop-shadow(5px 5px 5px rgba(0, 102, 81, 1));
    font-family: Segoe UI;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    border: none;
    background-color: rgba(0,204,162,1) !important;

}
 .active{
    background-color: rgb(7, 131, 106) !important;
}
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <button onclick="location.href='{{route('supervisor.daily.check') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-7 col-xs-7 col-md-7">
        <p class="text-center font-weight-bold" style="font-size: 130%">Revisión  de verificación diaria</p>
    </div>
  </div>
<!--Fin Encabezado inicial -->
<div class="ml-3 d-flex">
    <div class="input-group">
    <label for="">Indique la fecha</label>
    <input type="date" name="date" class="form-control col-6" value="{{$date}}" onchange="select_date(this)">
    </div>
</div>
<form action="{{route('store.check')}}" method="POST" onsubmit="save(event)" id="form_check">

<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
    
        @php
        $count=1;   
       @endphp
    
        @foreach ($options as $option)
        <div class="ID1_Cmo_te_sientes_hoy_Indica_ mb-4">
        <span class="font-gr"> {{$count}}.{{$option->name}}</span>
            <div class="d-flex justify-content-around">
                <div class="col-3 col-sm-3">
                    <input type="radio" disabled checked >SI
                </div>
                <div class="col-3 col-sm-3">
                    <input type="radio" disabled   checked >NO
                </div>
            </div>

            <div class="d-flex justify-content-around">
                @php
                $aux=$total;
                $row=0;
                $link=route('supervisor.daily.check.review.yes',['id'=>$option->id,'date'=>$date]);

                 @endphp
                   @foreach ($reviews as $review)
                   @php
                   @endphp
                   @if ($review->id == $option->id)
                   @php
                   $row=$review->filas;
                   $aux=(int) $total-$review->filas;
                    @endphp
                   @else
                   @endif
               @endforeach
               
            <a style="color: #000000" href="{{$link}}">ver {{$row}} respuestas</a>
            <a style="color: #000000" href="{{route('supervisor.daily.check.review.no',['id'=>$option->id,'date'=>$date])}}" > Ver
            {{$aux}} respuestas</span>
            </div>
        </div>
        @php
         $count=$count+1;   
        @endphp
        @endforeach
            @csrf
        <div id="option"></div>
        <br>


    </div>
		
</div>
</section>

@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    function select_date(a) {
       url='{{route('supervisor.daily.check.review',["date"=>0])}}';
       url=url.replace('0',$(a).val());
       location.href=url;
    }
</script>
@endsection


