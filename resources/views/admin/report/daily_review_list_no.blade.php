@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .card{
       border-radius: 10% !important;
       background-color: rgba(204,204,204,0.8) !important;
   }
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <button onclick="location.href='{{route('supervisor.daily.check.review') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-7 col-xs-7 col-md-7">
        <p class="text-center font-weight-bold" style="font-size: 130%">Respuestas de verificación diaria</p>
    </div>
  </div>
<!--Fin Encabezado inicial -->
<form action="{{route('store.check')}}" method="POST" onsubmit="save(event)" id="form_check">

<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
        <hr class="line-sep">
        @foreach ($reviews as $review)

        <div>
            <div>
                <span>Registrado por:</span>
            <span>{{$review->name}}</span>
            </div>
        <div>
            <span>Fecha:</span>
            <span>{{date('Y-m-d', strtotime($review->created_at))}}</span>
        </div>
        <div>
            <span>Hora:</span>
            <span>{{date('H:i:s', strtotime($review->created_at))}}</span>
        </div>
        <hr class="line-sep">
        </div>
        @endforeach
            @csrf
        <div id="option"></div>
        <br>
        <div class="mx-auto">
            <ul class="pagination ">
            <li ><a class="last_page btn " href="javascript:;">Página {{$reviews->currentPage()}} de {{$reviews->lastPage()}}</a></li>
                <li ><a class="last_page btn btn-color" style="background-color: rgba(67,32,255,1);color:#ffffff;border-radius: 10px;" href="{{$reviews->previousPageUrl() }}"><i class="fa fa-chevron-left"></i></a></li>
                <li><a class="next_page btn btn-color" style="background-color: rgba(67,32,255,1);color:#ffffff; border-radius: 10px;" href="{{$reviews->nextPageUrl() }}"><i class="fa fa-chevron-right"></i></a></li>
              </ul>
        </div>
 
    </div>
		
</div>
</section>
<section id="section_verification" style="display: none">
    @include('public.partials.confirm')

</section>
</form>

<section id="section_success" style="display: none">
    @include('public.partials.success')
</section>
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    var option=[];
    function check(a) {
        let id=$(a).data('id_option');
        let val=$(a).data('val');
        if (val==1) {        
             $("#option").append('<input type="hidden" value="'+id+'" name="option[]">');

            $("."+id).removeClass('active');
            $(a).addClass('active');
        }else{
            $("."+id).removeClass('active');
            $(a).addClass('active');
        }
    }
    function send() {
        $("#section_check").css('display','none');
        $("#section_verification").css('display','block');
    }
    function save(e) {
       e.preventDefault();
       var formdata = new FormData($("#form_check")[0]);
       $.ajax({
           url         : "{{route('store.check')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
           swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
               }
             }
       }); 
 }
 function back() {
     
    $("#section_check").css('display','block');
    $("#section_verification").css('display','none');

 }
</script>
@endsection


