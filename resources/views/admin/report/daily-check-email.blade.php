@extends('layouts.views')
@section('css')
<style>
	
	#Rectngulo_49 {
		fill: rgba(0,204,170,0.498);
	}
	.Rectngulo_49 {
		position: absolute;
		overflow: visible;
		width: 200px;
        height: 200px;
        left: 20%;
        top: 18%;
}
	}
    
	#Rect {
		fill: rgba(0,204,170,0.498);
	}
	.Rectngulo_50 {
		position: absolute;
		overflow: visible;
		width: 200px;
        height: 200px;
        left: 20%;
        top: 60%;
        fill: rgba(0,204,170,0.498)

	}
	#Icon_awesome-clipboard-list {
		fill: rgba(255,255,255,1);
	}
	.Icon_awesome-clipboard-list {
		overflow: visible;
		position: relative;
        width: 40%;
        height: 40%;
		margin-top: 0;

		transform: matrix(1,0,0,1,0,0);
	}

	#Icon_material-find-in-page {
		fill: rgba(255,255,255,1);
	}
	.Icon_material-find-in-page {
		overflow: visible;
		position: relative;
		width: 40%;
        height: 40%;
		transform: matrix(1,0,0,1,0,0);
	}
	.Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 90%;
		height: 5%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn{
	    white-space: nowrap;
		text-align: center;
		font-family: Segoe UI;
		font-style: normal;
		font-weight: bold;
		font-size: 130%;
		color: rgba(255,255,255,1);
		background-color: rgba(0,170,204,0.498);
		width: 65%;
		height: 95%;
   }
   #Icon_simple-microsoftexcel {
		fill: rgba(255,255,255,1);
	}
	.Icon_simple-microsoftexcel {
		overflow: visible;
		position: relative;
		
		transform: matrix(1,0,0,1,0,0);
	}
    #Enviar {
    white-space: nowrap;
    text-align: center;
    font-family: SegoeUI;
    font-style: normal;
    font-weight: normal;
    font-size: 20px;
    color: rgba(255,255,255,1);
    border-radius: 7px !important;
    
}
   </style>

@endsection
@section('content')
<section id="section_verification">


<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-2 col-sm-2 col-md-2">
        <button onclick="location.href='{{route('admin.daily.check.chart') }}'" style="background-color: transparent; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-8 col-sm-8 col-md-8 text-center align-self-center">
        <p class="text-center font-weight-bold" style="font-size: 110%">Revisión  de verificación diaria</p>
	</div>
    <div class="col-2 col-sm-2 col-md-2">
	</div>
  </div>
  
	<div class="row ">

		<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
		</div>
	
		<div class="col-10 col-sm-10 text-center">
			<h5 class="text-center text-light" >Exportación a excel</h5>
			<p>Por favor, ingrese su correo electrónico para poder
			enviarle la tabla de datos de esta función
			 registrados hasta la fecha de hoy.</p>
			 <br><br>
				 <div class="form-group text-center">
					 <input type="text" class="form-control" name="email" id="email" type="mail" placeholder="correo electrónico">
				 </div>
				 <div class="form-group text-center mb-4">
					<button class="btn " style="background-color: rgba(67,32,255,1) !important; margin-bottom: 30px;"  type="button" onclick="send();" id="btn_enviar">Enviar</button>
	
				 </div>
				 <div class="form-group mt-4">
					<a href="#" class="text-center "><img src="{{asset('img/logo.png')}}"  style="width: 40%"></a>
	
				 </div>
	
		</div>
		<div class="col-lg-1 col-md-1 col-xs-1 col-1 mr-2">
		</div>
	  </div>
</section>

<section id="section_success" style="display: none">
	@include('admin.partials.success')

</section>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
	
function send(){
		let url="{{route('admin.daily.check.send.email',0)}}";
		url=url.replace('0',$("#email").val())
       $.ajax({
           url         : url,
           type        : 'GET',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
       
       }); 
}
</script>	
@endsection


