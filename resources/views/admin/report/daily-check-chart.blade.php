@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 100%;
		height: 5%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   .btn-success{
    width: 100px;
    height: 30px;
    filter: drop-shadow(5px 5px 5px rgba(0, 102, 81, 1));
    font-family: Segoe UI;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    border: none;
    background-color: rgba(0,204,162,1) !important;

}
 .active{
    background-color: rgb(7, 131, 106) !important;
}
#Icon_simple-microsoftexcel {
		fill: rgba(255,255,255,1);
	}
	.Icon_simple-microsoftexcel {
		overflow: visible;
		position: relative;
		width: 65%;
        height: 55%;
		transform: matrix(1,0,0,1,0,0);
	}
   </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-1 col-sm-1 col-md-1">
        <button onclick="location.href='{{route('admin.daily.check') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
			<i class="fa fa-arrow-left fa-2x" style="color:#ffffff;"></i>
        </button>
    </div>
    <div class="col-10 col-sm-10 col-md-10 align-self-center">
        <h5 class="text-center font-weight-bold " style="font-size: 100%;color:#ffffff">Estadísticas  de verificación diaria</h5>
    </div>
    <div class="col-1 col-sm-1 col-md-1">
    </div>

  </div>
<!--Fin Encabezado inicial -->
<label for="" class="ml-3">Indique la fecha:</label>

<div class="ml-3 d-flex">

    <div class="input-group">
    <input type="date" name="date" class="form-control col-8" value="{{$date}}" onchange="select_date(this)">
    <div class="col-4 col-lg-4">
        <a href="{{route('admin.daily.check.email')}}">
            <svg class="Icon_simple-microsoftexcel" viewBox="0 0.329 72.5 71.177">
				<path id="Icon_simple-microsoftexcel" d="M 71.14968872070313 9.037563323974609 L 46.8621826171875 9.037563323974609 L 46.8621826171875 13.53256225585938 L 54.02155685424805 13.53256225585938 L 54.02155685424805 20.64360427856445 L 46.8621826171875 20.64360427856445 L 46.8621826171875 22.90922927856445 L 54.02155685424805 22.90922927856445 L 54.02155685424805 30.02933311462402 L 46.8621826171875 30.02933311462402 L 46.8621826171875 32.35839462280273 L 54.02155685424805 32.35839462280273 L 54.02155685424805 39.09183502197266 L 46.8621826171875 39.09183502197266 L 46.8621826171875 41.7894401550293 L 54.02155685424805 41.7894401550293 L 54.02155685424805 48.53797912597656 L 46.8621826171875 48.53797912597656 L 46.8621826171875 51.2355842590332 L 54.02155685424805 51.2355842590332 L 54.02155685424805 58.02340316772461 L 46.8621826171875 58.02340316772461 L 46.8621826171875 62.97455215454102 L 71.14968872070313 62.97455215454102 C 71.53333282470703 62.85976028442383 71.85353851318359 62.40663528442383 72.11032104492188 61.62121200561523 C 72.36708068847656 60.82976150512695 72.5 60.18631744384766 72.5 59.69996643066406 L 72.5 9.85922908782959 C 72.5 9.472562789916992 72.36708068847656 9.239958763122559 72.11032104492188 9.158395767211914 C 71.85353851318359 9.079854965209961 71.53334045410156 9.037563323974609 71.14968872070313 9.037563323974609 Z M 67.99895477294922 58.01736450195313 L 56.34760665893555 58.01736450195313 L 56.34760665893555 51.2355842590332 L 67.99895477294922 51.2355842590332 L 67.99895477294922 58.02340316772461 L 67.99895477294922 58.01736450195313 Z M 67.99895477294922 48.53798675537109 L 56.34760665893555 48.53798675537109 L 56.34760665893555 41.78642272949219 L 67.99895477294922 41.78642272949219 L 67.99895477294922 48.53798675537109 Z M 67.99895477294922 39.08882141113281 L 56.34760665893555 39.08882141113281 L 56.34760665893555 32.38558578491211 L 67.99895477294922 32.38558578491211 L 67.99895477294922 39.0948600769043 L 67.99895477294922 39.08882141113281 Z M 67.99895477294922 30.02631759643555 L 56.34760665893555 30.02631759643555 L 56.34760665893555 22.9152717590332 L 67.99895477294922 22.9152717590332 L 67.99895477294922 30.02933311462402 L 67.99895477294922 30.02631187438965 Z M 67.99895477294922 20.60433769226074 L 56.34760665893555 20.60433769226074 L 56.34760665893555 13.53558921813965 L 67.99895477294922 13.53558921813965 L 67.99895477294922 20.6496524810791 L 67.99895477294922 20.60433769226074 Z M 0 7.916833400726318 L 0 64.10736083984375 L 42.77500152587891 71.50537872314453 L 42.77500152587891 0.3285000324249268 L 0 7.940999507904053 L 0 7.916833400726318 Z M 25.35385704040527 50.4229850769043 C 25.19072914123535 49.98194122314453 24.42343902587891 48.10902404785156 23.06406402587891 44.79516983032227 C 21.70771026611328 41.48433303833008 20.88906478881836 39.55704498291016 20.65041923522949 39.00725173950195 L 20.57489776611328 39.00725173950195 L 15.98625183105469 49.92756271362305 L 9.853959083557129 49.51371383666992 L 17.12812614440918 35.91996002197266 L 10.46718883514404 22.32621192932129 L 16.72031211853027 21.99693870544434 L 20.85281181335449 32.63329696655273 L 20.93437576293945 32.63329696655273 L 25.6015625 21.51360702514648 L 32.06312561035156 21.10579299926758 L 24.36906242370605 35.81725311279297 L 32.29875183105469 50.82475280761719 L 25.35385704040527 50.41694259643555 L 25.35385704040527 50.4229850769043 Z">
				</path>
            </svg>            
        </a>
    </div>    
</div>


</div>
<form action="{{route('store.check')}}" method="POST" onsubmit="save(event)" id="form_check">

<section id="section_check">
<div class="opa-rec" >
    <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
    
        @php
        $count=1;   
       @endphp
    
        @foreach ($options as $option)
        <div class="ID1_Cmo_te_sientes_hoy_Indica_ mb-4">
        <span class="font-gr"> {{$count}}.{{$option->name}}</span>
            <div class="d-flex justify-content-around">
               <canvas id="chart-{{$option->id}}" class="canvas_{{$option->id}}" width="100vh" height="100vh"></canvas>
            </div>
            <div class="row justify-content-around">
                <span id="response_si_{{$option->id}}"></span>
                    <span id="response_no_{{$option->id}}"></span>
            </div>
            <div class="d-flex justify-content-around">
                <span id="response_total_{{$option->id}}"></span>
     
            </div>
        </div>
        @php
         $count=$count+1;   
        @endphp
        @endforeach
            @csrf
        <div id="option"></div>
        <br>


    </div>
		
</div>
</section>

@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/chart.js')}}"></script> 

<script>
    function select_date(a) {  
       url='{{route('admin.daily.check.chart',["date"=>0])}}';
       url=url.replace('0',$(a).val());
       location.href=url;
    }
    
    @foreach ($options as $option)
    
    @php
                $aux=$total;
                $row=0;
                $yes=0;
                $no=0;

                $link=route('supervisor.daily.check.review.yes',['id'=>$option->id,'date'=>$date]);

                 @endphp
                   @foreach ($reviews as $review)
                   @php
                   @endphp
                   @if ($review->id == $option->id)
                   @php
                   $row=$review->filas;
                   $aux=(int) $total-$review->filas;
                   $yes=number_format(($row / $total) * 100,1);
                   $no=number_format(($aux / $total) * 100,1);
                   @endphp
                   @else
                   @endif
               @endforeach
    var ctx{{$option->id}} = document.getElementById('chart-{{$option->id}}');
    var myChart{{$option->id}} = new Chart(ctx{{$option->id}}, {
        type: 'doughnut',
         data: {
            labels: ['Si  {{$yes}}%','No {{$no}}%'],

                datasets: [{
                    data: [{{$yes}}, {{$no}},],
                    backgroundColor: [
                        'rgba(0,204,162,1)',
                        'rgba(67,32,255,1)',
                    ],

                    borderWidth: 1
                }]
            },
            options: {

        legend: {
            display: true,
            labels: {
                fontColor: 'black'
            },
            tooltips: {
                intersect:false,

            }
        }
    }
        });
    $("#response_si_{{$option->id}}").text('{{$row}}'+' respuestas');
    $("#response_no_{{$option->id}}").text('{{$aux}}'+' respuestas');
    $("#response_total_{{$option->id}}").text('{{$total}}'+' respuestas totales');

    @endforeach

//    var myChart = new Chart(ctx, {


</script>
@endsection


