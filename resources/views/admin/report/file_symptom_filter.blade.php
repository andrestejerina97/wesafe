@extends('layouts.views')
@section('css')
<style>
    
    .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   </style>
@endsection
@section('content')
<div class="row mb-4 mt-4 bg-secondary text-white ">
    <div class="col-3 col-xs-3 col-md-3">
        <a href="{{route('supervisor.file.review')}}">
            <svg  class="Icon_awesome-arrow-left" viewBox="-9.004 -0 10 10" >
                <path id="Icon_awesome-arrow-left"  d="M 41.50252532958984 68.31076812744141 L 37.92483139038086 71.88845062255859 C 36.40995788574219 73.40333557128906 33.96036529541016 73.40333557128906 32.46160125732422 71.88845062255859 L 1.13264262676239 40.57561111450195 C -0.382235050201416 39.06072998046875 -0.382235050201416 36.61114883422852 1.13264262676239 35.11238479614258 L 32.46160125732422 3.78342342376709 C 33.97648239135742 2.268545866012573 36.42607116699219 2.268545866012573 37.92483139038086 3.78342342376709 L 41.50252532958984 7.361112594604492 C 43.03351593017578 8.892107009887695 43.00128173828125 11.39004421234131 41.43805694580078 12.88880634307861 L 22.01861572265625 31.3896541595459 L 68.33519744873047 31.3896541595459 C 70.47857666015625 31.3896541595459 72.20296478271484 33.11403274536133 72.20296478271484 35.25742340087891 L 72.20296478271484 40.41445541381836 C 72.20296478271484 42.55784606933594 70.47857666015625 44.28223037719727 68.33519744873047 44.28223037719727 L 22.01861572265625 44.28223037719727 L 41.43805694580078 62.7830696105957 C 43.01739501953125 64.28183746337891 43.04962921142578 66.77977752685547 41.50252532958984 68.31076812744141 Z">
                </path>
            </svg>
        </a>
    </div>
    <div class="col-7 col-xs-7 col-md-7">
        <p class="text-center font-weight-bold">Fichas sintomatológicas registradas</p>
    </div>
</div>

<form  method="POST" onsubmit="save(event)" action="{{route('store.sworn')}}"  id="form_check">
@csrf
    <section id="section_check">
<div class="opa-rec" >
        <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
            <div>
                <div class="Informacion_Personal">
                    <span class="font-gr">Información del trabajador</span>
                </div>
            </div>
                                <hr class="line-sep"> 
            
                                <div>
                                    <div>
                                        <span>Registrado por:</span>
                                    <span>{{$reviews->name}}</span>
                                    </div>
                                <div>
                                    <span>Fecha:</span>
                                    <span>{{date('Y-m-d', strtotime($reviews->created_at))}}</span>
                                </div>
                                <div>
                                    <span>Hora:</span>
                                    <span>{{date('H:i:s', strtotime($reviews->created_at))}}</span>
                                </div>
                                <hr class="line-sep">
                                </div>

                <div class="text-center">
                    <span class="font-gr"> A través de la presente declaración jurada, se compromete a responder con la verdad.¿En los últimos 14 días calendario ha tenido alguno de los siguientes síntomas?</span>
                </div>
                @php
                $count=1;   
               @endphp
                @foreach ($options as $option)
                    
                
                    <div class="ID1_Cmo_te_sientes_hoy_Indica_ pl-4">
                        <span class="font-gr"> {{$count}} {{$option->name}} </span>
                    
             

                                @if($option->name=="Está tomando alguna medicación")
                                <div class="d-flex mx-auto">
                                    <div class="col-3 col-sm-3">
                                        <input type="radio" @if (in_array($option->id,$array_option)) checked @endif>SI</button>
                                    </div>
                                    <div class="col-3 col-sm-3">
                                        <input type="radio" @if (in_array($option->id,$array_option)) @else checked @endif">NO</button>

                                    </div>
                                    <div class="col-6 col-sm-6">
                                        <div class=" input-group-sm mb-1">
                                        <input type="text" class="form-control" name="input_medicine" id="input_medicine" disabled value=" @if($reviews->medicines != '') {{$reviews->medicines}} @endif" aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                    </div>
                                </div>
                        

                                @else
                                <div class=" d-flex ">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <label for="" class="check-radio">Si</label>
                                            <input type="radio" @if (in_array($option->id,$array_option)) checked @endif>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <label for="" class="check-radio">No</label>
                                            <input type="radio" @if (in_array($option->id,$array_option)) @else checked @endif>
    
                                        </div>
                                    </div>
           
                                
                                </div>
                                @endif

                    </div>
                    @php
                    $count++;   
                   @endphp
                @endforeach
             
                    
                    
                        <div class="text-center">
                            <span class="font-gr"> Todos los datos expresados en este cuestionario constituyen una declaración jurada de su parte.
                            Asimismo, ha sido informado que, de omitir o falsear información, puede perjudicar la salud de sus compañeros y la mía propia, lo cual, de constituir una falta grave a la salud pública, asume sus consecuencias.</span>
                        
                        </div>

                
                 
    </div>	

</div>
</section>


@endsection




