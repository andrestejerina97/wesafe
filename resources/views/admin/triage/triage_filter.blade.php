@extends('layouts.views')
@section('css')
    <style>
      
      .line-sep{
       border-style: solid;
       background-color: blue;
       height: 2px;
    }

   .opa-rec {
       opacity: 0.9;
       color: black;
       margin: 15px;
      
   }

   .font-gr{
       font-weight: 750;
   }
   
   .Informacion_Personal {
       filter: drop-shadow(3px 3px 3px rgba(0, 0, 0, 0.2));
       overflow: visible;
       white-space: nowrap;
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 18px;
       color: rgba(0,0,0,1);
   }

   .ID1_Cmo_te_sientes_hoy_Indica_ {
       filter: drop-shadow(2px 2px 2px rgba(0, 0, 0, 0.2));
       text-align: left;
       font-family: Segoe UI;
       font-style: normal;
       font-weight: bold;
       font-size: 13px;
       color: rgba(0,0,0,1);
   }

   .rect-ps1{
       position: relative;
       top: 40px; left: 40px;
   }

   .btn.btn-primary {
       background: #007eea !important;
       border-color: #007eea !important;
   }
   .si-primer-pregunta{
        display: flex;
   }
   .Icon_awesome-arrow-left{
        overflow: visible;
		position: relative;
		width: 7%;
		height: 7%;
        left: 20%;
        top: -5%;
        margin: 0%;
        color: azure !important;
   }
   #Icon_awesome-arrow-left{
    fill: rgba(255,255,255,1);

   }
   #Icon_awesome-arrow-left:hover{
    fill: rgb(24, 21, 21);

   }
   
    .otro-pregunta-5{
    display: flex;
   }
   .title{
    font-size: 130%;
    margin-left: 10%;

}
   
@media (max-width: 350px) {
 
    .title{
    font-size: 120%;
}
}
    </style>
@endsection
@section('content')
<!--Encabezado inicial -->

<div class="row mb-4 mt-4 bg-secondary text-white ">
  <div class="col-2 col-sm-2 col-md-2">
      <button onclick="location.href='{{route('supervisor.triage.review') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-left">
    <i class="fa fa-arrow-left fa-2x" aria-hidden="true" style="color:#ffffff;"></i>
      </button>
  </div>
  <div class="col-8 col-sm-8 col-md-8 ">
    <p class="text-center font-weight-bold title">Triaje diario registrado</p>
  </div>
  <div class="col-2 col-sm-2 col-md-2">
    <button onclick="location.href='{{route('admin.triage.chart') }}'" style="background-color: transparent; width:100%; height:100%; border:none;" type="button" class="text-right">
			<i class="fa fa-bar-chart fa-2x" aria-hidden="true" style="color:#ffffff;"></i>
        </button>
  </div>
</div>
<!--Fin Encabezado inicial -->


        <form  method="POST" action="{{route('store.triage')}}" onsubmit="save(event)" id="form_check">
            @csrf
            <section  id="section_check" >

              <div class="opa-rec" >
                <div  class="p-3 bg-secondary card text-black" id="mb-inferior">
               
              @include('supervisor.partials.data-triage')
           
            <input type="hidden" value="No" id="contact" name="contact">

          
</div>

</div>
</section>

<section  id="section_verification" style="display: none">
  @include('public.partials.confirm')

</section>
</form>
<section id="section_success" style="display: none">
  @include('public.partials.success')
  </section>
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script> 

<script>
    var option=[];
    function change_other(a){
        if ($(a).prop('checked')==true) {
        $("#input_symptom").attr("disabled",false);

        }else{
            $("#input_symptom").attr("disabled",true);
            $("#input_symptom").val('');

        }
    }
    function change_contact(a) {
      if ($(a).text()=='SI') {
          $(a).addClass('active');
          $("#btn_contact_no").removeClass('active');
          $("#contact").val('Si')
      }else{
        $(a).addClass('active');
        $("#btn_contact_si").removeClass('active');
        $("#contact").val('No')

      }
    }
   
    function send() {
        $("#section_check").css('display','none');
        $("#section_verification").css('display','block');
    }
    function save(e) {
       e.preventDefault();
       var formdata = new FormData($("#form_check")[0]);
       $.ajax({
           url         : "{{route('store.triage')}}",
           data        : formdata,
           cache       : false,
           contentType : false,
           processData : false,
           type        : 'POST',
           dataType:"JSON",
           beforeSend:function() {
             swal("Por favor espera,tu petición se está procesando!", {
               buttons: false,
               
             });
           },
           success: function(data, textStatus, jqXHR){
               swal.close();
               if (data.result != null) {
                 let msg= "La información se ha registrado correctamente";
                 $("#date_success").text(data.date);
                 $("#time_success").text(data.time);
                 $("#section_verification").css('display','none');
                 $("#section_success").css('display','block');
               }else{
                 swal("Ups!",data.message,"error");   
               }

             },
             error:function(data,message,res){
               swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
             },
             statusCode:{
               422:function(data) {
           swal("Ups!","El número de caso ingresado ya existe,por favor ingrese uno nuevo","error");
               }
             }
       }); 
 }
 function back() {
     
    $("#section_check").css('display','block');
    $("#section_verification").css('display','none');

 }
</script>
@endsection
