				
                
                		<div class="col-xs-12 hidden-lg hidden-sm hidden-md text-center">	
					
                            <div class="row wow animated bounceInLeft box2" data-wow-delay="0.2s">
                                <br>
                                <br>
                                <br>
                                
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                 
                                </div>
                                
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                        <a href="" data-toggle="modal" data-target="#casos" onclick="filter_caso();"><p><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                                            <span style="font-size: 8px; line-height: 1;"><br>CASOS PARA<br>AYUDAR</span></p></a>
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <a href="" data-toggle="modal" data-target="#proyectos" onclick="filter_proyecto();"><p><i class="fa fa-globe fa-2x" aria-hidden="true"></i> <span style="font-size: 8px"><br>PROYECTOS<br>COMUNITARIOS</span></a>
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <a href="" data-toggle="modal" data-target="#oportunidad" onclick="filter_oportunidad();"><p><i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>	
                                        <span style="font-size: 8px"><br>DONACIONES Y<br>OPORTUNIDADES</span></p></a>
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <a href="" data-toggle="modal" data-target="#puente" onclick="filter_puente();"><p><i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                                        <span style="font-size: 8px"><br>PUENTES<br>REALIZADOS</span></p></a>
    
                                </div>
    
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <a href="subir-caso.html"><p><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i><span style="font-size: 8px"><br>SUBIR<br>CASO</span></p></a>
                                </div>	
                                
                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                 
                                </div>
                                
    
                            </div>
                    </div>
                <div class="col-md-12 col-sm-12 col-lg-12 hidden-xs text-center">	
					
                    <div class="row wow animated bounceInLeft box2" data-wow-delay="0.2s">
                        <br>
                        <br>
                        <br>
                        
                        <div class="col-md-1 col-sm-1 col-xs-1">
                                         
                        </div>
                        
                        <div class="col-md-2 col-sm-2 col-xs-2">
                                <a href="" data-toggle="modal" data-target="#casos" onclick="filter_caso();"><p><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                                    <br><small>CASOS PARA<br>AYUDAR</small></p></a>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <a href="" data-toggle="modal" data-target="#proyectos" onclick="filter_proyecto();"><p><i class="fa fa-globe fa-2x" aria-hidden="true"></i> <br><small>PROYECTOS<br>COMUNITARIOS</small></a>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <a href="" data-toggle="modal" data-target="#oportunidad" onclick="filter_oportunidad();"><p><i class="fa fa-bullhorn fa-2x" aria-hidden="true"></i>	
                                <br><small>DONACIONES Y<br>OPORTUNIDADES</small></p></a>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <a href="" data-toggle="modal" data-target="#puente" onclick="filter_puente();"><p><i class="fa fa-heart fa-2x" aria-hidden="true"></i>
                                <br><small>PUENTES<br>REALIZADOS</small></p></a>

                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <a href="subir-caso.html"><p><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i><br><small>SUBIR<br>CASO</small></p></a>
                        </div>	
                        
                        <div class="col-md-1 col-sm-1 col-xs-1">
                                         
                        </div>
                        

                    </div>
            </div>
            
            
        </div>